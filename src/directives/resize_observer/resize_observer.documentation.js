import description from './resize_observer.md';

export default {
  followsDesignSystem: false,
  description,
};
