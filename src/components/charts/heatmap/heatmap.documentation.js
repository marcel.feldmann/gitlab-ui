import description from './heatmap.md';

export default {
  description,
  followsDesignSystem: true,
};
