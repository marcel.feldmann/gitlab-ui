import description from './stacked_column.md';

export default {
  followsDesignSystem: false,
  description,
};
