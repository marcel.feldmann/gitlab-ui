import * as description from './filtered_search.md';

export default {
  description,
  followsDesignSystem: true,
};
