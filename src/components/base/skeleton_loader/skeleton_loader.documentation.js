import * as description from './skeleton_loader.md';

export default {
  followsDesignSystem: true,
  description,
};
