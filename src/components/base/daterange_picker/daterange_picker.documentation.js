import description from './daterange_picker.md';

export default {
  followsDesignSystem: true,
  description,
};
