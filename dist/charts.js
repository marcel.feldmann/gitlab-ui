import setConfigs from './config';
export { default as GlChart } from './components/charts/chart/chart';
export { default as GlAreaChart } from './components/charts/area/area';
export { default as GlBarChart } from './components/charts/bar/bar';
export { default as GlGaugeChart } from './components/charts/gauge/gauge';
export { default as GlLineChart } from './components/charts/line/line';
export { default as GlChartLegend } from './components/charts/legend/legend';
export { default as GlChartSeriesLabel } from './components/charts/series_label/series_label';
export { default as GlChartTooltip } from './components/charts/tooltip/tooltip';
export { default as GlHeatmap } from './components/charts/heatmap';
export { default as GlColumnChart } from './components/charts/column/column';
export { default as GlStackedColumnChart } from './components/charts/stacked_column/stacked_column';
export { default as GlDiscreteScatterChart } from './components/charts/discrete_scatter/discrete_scatter';
export { default as GlSingleStat } from './components/charts/single_stat/single_stat';
export { default as GlSparklineChart } from './components/charts/sparkline/sparkline';

// Add config files
setConfigs();
