import { buttonSizeOptionsMap } from '../../utils/constants';

const ButtonMixin = {
  computed: {
    buttonSize() {
      return buttonSizeOptionsMap[this.size];
    }

  }
};

export { ButtonMixin };
