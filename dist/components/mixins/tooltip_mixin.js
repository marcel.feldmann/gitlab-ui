import { tooltipActionEvents } from '../../utils/constants';

var tooltip_mixin = (tooltipRefName => ({
  mounted() {
    /**
     * Pass through the events to programmatically open, close, enable
     * and disable a tooltip or a popover.
     *
     * References
     * https://bootstrap-vue.org/docs/components/popover#programmatically-show-and-hide-popover
     * https://bootstrap-vue.org/docs/components/tooltip#programmatically-show-and-hide-tooltip
     */
    tooltipActionEvents.forEach(event => this.$on(event, () => this.$refs[tooltipRefName].$emit(event)));
  },

  beforeDestroy() {
    tooltipActionEvents.forEach(event => this.$off(event));
  }

}));

export default tooltip_mixin;
