var gauge = "Some layout problems can be encountered with this component. Specifically, when the gauge chart's\naxis labels or detail text have larger widths, they can overlap with the chart elements.\n\nAlso, when the containing element of the gauge chart has either of a small calculated `width` and\n`height`, its axis width could become bulkier in relation to other chart elements. This is because\nat this time [eCharts only allows for setting the axis line width in absolute units](https://echarts.apache.org/en/option.html#series-gauge.axisLine.lineStyle.width).\n\nThis issue is to be addressed by [https://gitlab.com/gitlab-org/gitlab-ui/-/issues/916](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/916).\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': gauge
});

var gauge_documentation = {
  description
};

export default gauge_documentation;
