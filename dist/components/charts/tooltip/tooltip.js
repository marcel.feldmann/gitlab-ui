import * as echarts from 'echarts';
import { uid } from '../../../utils/utils';
import GlPopover from '../../base/popover/popover';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  components: {
    GlPopover
  },
  inheritAttrs: false,
  props: {
    chart: {
      type: Object,
      required: true,

      validator(chart) {
        return Object.is(chart, echarts.getInstanceByDom(chart.getDom()));
      }

    },
    id: {
      type: String,
      required: false,
      default: () => uid()
    },
    top: {
      type: String,
      required: false,
      default: null
    },
    bottom: {
      type: String,
      required: false,
      default: null
    },
    left: {
      type: String,
      required: false,
      default: null
    },
    right: {
      type: String,
      required: false,
      default: null
    }
  },
  computed: {
    containerId() {
      // if multiple tooltips are used in a chart component,
      // `this.id` can be used to uniquely identify them
      return `${this.chart.getDom().getAttribute('_echarts_instance_')}-tooltip-${this.id}`;
    },

    containerPosition() {
      const props = ['top', 'bottom', 'left', 'right'];
      return props.reduce((accumulator, prop) => {
        const position = this[prop];

        if (position) {
          accumulator[prop] = position;
        }

        return accumulator;
      }, {});
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"gl-chart-tooltip",staticStyle:{"width":"1px","height":"1px"},style:(_vm.containerPosition),attrs:{"id":_vm.containerId}}),_vm._v(" "),_c('gl-popover',_vm._b({attrs:{"target":_vm.containerId,"container":_vm.containerId,"triggers":""},scopedSlots:_vm._u([(_vm.$scopedSlots.title)?{key:"title",fn:function(){return [_vm._t("title")]},proxy:true}:null],null,true)},'gl-popover',_vm.$attrs,false),[_vm._v(" "),_vm._t("default")],2)],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
