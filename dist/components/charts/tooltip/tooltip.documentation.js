var description = "### ECharts custom tooltip\n\nUses GitLab UI's `popover` component in lieu of echart's tooltip.\n";

var tooltip_documentation = {
  followsDesignSystem: false,
  description
};

export default tooltip_documentation;
