var description = "### ECharts Wrapper\n\nThe chart component is a Vue component wrapper around [ECharts]. The chart component accepts width\nand height props in order to allow the user to make it responsive, but it is not responsive by\ndefault.\n\n> Note: In every case there should be a specific component for each type of chart\n(i.e. Line, Area, Bar, etc.). This component should only need to be used by chart type components\nwithin GitLab UI as opposed to being used directly within any other codebase.\n\n### EChart Lifecycle\n\nThis component emits the following events during the ECharts lifecycle:\n\n- `created`: emitted after calling `echarts.init`\n- `updated`: emitted after calling `echarts.setOption`\n\nIn all cases, the event payload is the echart instance.\n\n[echarts]: https://echarts.apache.org\n";

var chart_documentation = {
  description
};

export default chart_documentation;
