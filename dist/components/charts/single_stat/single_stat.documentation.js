var description = "The single stat component is used to show a single value. The color of the meta icon / badge is\ndetermined by the **variant** prop, which can be one of \"success\", \"warning\", \"danger\", \"info\",\n\"neutral\" or \"muted\" (default).\n\n#### Hover state\n\nYou can make the component focusable by adding a `tabindex=0` attribute to it. This will also apply\na hover state to the component.\n";

var single_stat_documentation = {
  description
};

export default single_stat_documentation;
