var description = "Displays chart data series name as a label for chart legend, tooltip, etc.\n";

var series_label_documentation = {
  description
};

export default series_label_documentation;
