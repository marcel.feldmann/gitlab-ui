var description = "This is a basic line chart.\n\n### Implementation Details\n\nThis component wraps the Gitlab UI `chart` component, which in turn wraps the ECharts component.\n\nSee the [chart](./?path=/story/charts-chart--default) component for more info.\n";

var line_documentation = {
  followsDesignSystem: false,
  description
};

export default line_documentation;
