import merge from 'lodash/merge';
import defaultChartOptions, { symbolSize, lineStyle, getThresholdConfig, generateAnnotationSeries, dataZoomAdjustments, mergeSeriesToOptions, mergeAnnotationAxisToOptions, grid, getDefaultTooltipContent } from '../../../utils/charts/config';
import { LEGEND_AVERAGE_TEXT, LEGEND_MAX_TEXT, LEGEND_MIN_TEXT, LEGEND_CURRENT_TEXT, LEGEND_LAYOUT_INLINE, LEGEND_LAYOUT_TABLE, DATA_TOOLTIP_LEFT_OFFSET, ANNOTATION_TOOLTIP_TOP_OFFSET } from '../../../utils/charts/constants';
import { colorFromDefaultPalette } from '../../../utils/charts/theme';
import { seriesHasAnnotations, isDataPointAnnotation } from '../../../utils/charts/utils';
import { debounceByAnimationFrame } from '../../../utils/utils';
import ToolboxMixin from '../../mixins/toolbox_mixin';
import TooltipDefaultFormat from '../../shared_components/charts/tooltip_default_format';
import Chart from '../chart/chart';
import ChartLegend from '../legend/legend';
import ChartTooltip from '../tooltip/tooltip';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const gray200 = '#bfbfbf';

/**
 * Line charts as of %12.10 support annotations.
 * Annotations is composed of a dotted line and an arrow
 * at the bottom. The dotted line is constructed
 * with markLine and arrows with markPoint. Most of this
 * logic is in GitLab and should be eventually moved to
 * GitLab UI https://gitlab.com/gitlab-org/gitlab/-/issues/213390.
 *
 * Similar to how custom tooltips are displayed when line chart
 * is hovered, a tooltip should be displayed when the annotation
 * arrow is hovered. This component adds event listeners
 * to figure out if mouse is hovered on charts to show tooltips.
 * While that works for data points inside the grid, for arrows
 * that live right under the chart, we use eCharts inbuilt
 * event listeners to detect hover. Given this limitation,
 * we use a separate tooltip for data point and arrow.
 */
var script = {
  components: {
    Chart,
    ChartLegend,
    ChartTooltip,
    TooltipDefaultFormat
  },
  mixins: [ToolboxMixin],
  inheritAttrs: false,
  props: {
    data: {
      type: Array,
      required: true
    },
    option: {
      type: Object,
      required: false,
      default: () => ({})
    },
    thresholds: {
      type: Array,
      required: false,
      default: () => []
    },
    annotations: {
      type: Array,
      required: false,
      default: () => []
    },
    includeLegendAvgMax: {
      type: Boolean,
      required: false,
      default: true
    },
    formatTooltipText: {
      type: Function,
      required: false,
      default: null
    },
    legendAverageText: {
      type: String,
      required: false,
      default: LEGEND_AVERAGE_TEXT
    },
    legendMaxText: {
      type: String,
      required: false,
      default: LEGEND_MAX_TEXT
    },
    legendMinText: {
      type: String,
      required: false,
      default: LEGEND_MIN_TEXT
    },
    legendCurrentText: {
      type: String,
      required: false,
      default: LEGEND_CURRENT_TEXT
    },
    legendLayout: {
      type: String,
      required: false,
      default: LEGEND_LAYOUT_INLINE,

      validator(layout) {
        return [LEGEND_LAYOUT_INLINE, LEGEND_LAYOUT_TABLE].indexOf(layout) !== -1;
      }

    }
  },

  data() {
    // Part of the tooltip related data can be
    // moved into the tooltip component.
    // Tracking that progress in
    // https://gitlab.com/gitlab-org/gitlab-ui/-/issues/618
    return {
      chart: null,
      showDataTooltip: false,
      dataTooltipTitle: '',
      dataTooltipContent: {},
      dataTooltipPosition: {
        left: '0',
        top: '0'
      },
      showAnnotationsTooltip: false,
      annotationsTooltipTitle: '',
      annotationsTooltipContent: '',
      annotationsTooltipPosition: {
        left: '0',
        top: '0'
      },
      debouncedShowHideTooltip: debounceByAnimationFrame(this.showHideTooltip),
      selectedFormatTooltipText: this.formatTooltipText || this.defaultFormatTooltipText
    };
  },

  computed: {
    series() {
      const dataSeries = this.data.map((series, index) => {
        const defaultColor = colorFromDefaultPalette(index);

        const getColor = type => {
          var _series$type$color, _series$type;

          return (_series$type$color = (_series$type = series[type]) === null || _series$type === void 0 ? void 0 : _series$type.color) !== null && _series$type$color !== void 0 ? _series$type$color : defaultColor;
        };

        return merge({
          showSymbol: true,
          lineStyle: {
            color: getColor('lineStyle')
          },
          itemStyle: {
            color: getColor('itemStyle')
          }
        }, symbolSize, lineStyle, series, getThresholdConfig(this.thresholds));
      }); // if annotation series exists, append it
      // along with data series

      if (this.annotationSeries) {
        return [...dataSeries, this.annotationSeries];
      }

      return dataSeries;
    },

    annotationSeries() {
      return generateAnnotationSeries(this.annotations);
    },

    options() {
      const defaultLineChartOptions = {
        xAxis: {
          axisPointer: {
            show: true,
            label: {
              formatter: this.onLabelChange
            }
          },
          axisTick: {
            alignWithLabel: true,
            show: true,
            lineStyle: {
              color: gray200
            }
          }
        },
        legend: {
          show: false
        }
      };
      const mergedOptions = merge({}, defaultChartOptions, defaultLineChartOptions, this.option, dataZoomAdjustments(this.option.dataZoom), this.toolboxAdjustments); // All chart options can be merged but series
      // needs to be handled specially

      return mergeSeriesToOptions(mergeAnnotationAxisToOptions(mergedOptions, this.hasAnnotations), this.series);
    },

    /**
     * Annotations currently are passed as series options in monitoring dashboard.
     * Once https://gitlab.com/gitlab-org/gitlab/-/issues/213390 is closed,
     * annotations will be passed as props and not as series options.
     *
     * For backward compatibility, we're having to check for both.
     */
    hasAnnotations() {
      return this.annotations.length !== 0 || seriesHasAnnotations(this.option.series);
    },

    shouldShowAnnotationsTooltip() {
      return this.chart && this.hasAnnotations;
    },

    compiledOptions() {
      return this.chart ? this.chart.getOption() : null;
    },

    legendStyle() {
      return {
        paddingLeft: `${grid.left}px`
      };
    },

    seriesInfo() {
      return this.compiledOptions.series.reduce((acc, series, index) => {
        if (series.type === 'line') {
          acc.push({
            name: series.name,
            type: series.lineStyle.type,
            color: series.lineStyle.color || colorFromDefaultPalette(index),
            data: this.includeLegendAvgMax ? series.data.map(data => data[1]) : undefined
          });
        }

        return acc;
      }, []);
    }

  },

  beforeDestroy() {
    this.chart.getDom().removeEventListener('mousemove', this.debouncedShowHideTooltip);
    this.chart.getDom().removeEventListener('mouseout', this.debouncedShowHideTooltip);
    this.chart.off('mouseout', this.onChartDataPointMouseOut);
    this.chart.off('mouseover', this.onChartDataPointMouseOver);
  },

  methods: {
    defaultFormatTooltipText(params) {
      const {
        xLabels,
        tooltipContent
      } = getDefaultTooltipContent(params, this.options.yAxis.name);
      this.$set(this, 'dataTooltipContent', tooltipContent);
      this.dataTooltipTitle = xLabels.join(', ');
    },

    defaultAnnotationTooltipText(params) {
      var _params$data$tooltipD;

      return {
        title: params.data.xAxis,
        content: (_params$data$tooltipD = params.data.tooltipData) === null || _params$data$tooltipD === void 0 ? void 0 : _params$data$tooltipD.content
      };
    },

    onCreated(chart) {
      // These listeners are used to show/hide data tooltips
      // when the mouse is hovered over the parent container
      // of echarts' svg element. This works only for data points
      // and not markPoints.
      chart.getDom().addEventListener('mousemove', this.debouncedShowHideTooltip);
      chart.getDom().addEventListener('mouseout', this.debouncedShowHideTooltip); // eCharts inbuild mouse events
      // https://echarts.apache.org/en/api.html#events.Mouse%20events
      // is used to attach listeners to markPoints. These listeners
      // are currently used for annotation arrows at the bottom of the chart.
      // Because data points and annotations arrows are in different
      // sections of the charts with their own mouseovers and mouseouts,
      // there shouldn't be an overlapping situation where both tooltips
      // are visible.

      if (this.hasAnnotations) {
        chart.on('mouseout', this.onChartDataPointMouseOut);
        chart.on('mouseover', this.onChartDataPointMouseOver);
      }

      this.chart = chart;
      this.$emit('created', chart);
    },

    showHideTooltip(mouseEvent) {
      this.showDataTooltip = this.chart.containPixel('grid', [mouseEvent.zrX, mouseEvent.zrY]);
      this.dataTooltipPosition = {
        left: `${mouseEvent.zrX + DATA_TOOLTIP_LEFT_OFFSET}px`,
        top: `${mouseEvent.zrY}px`
      };
    },

    onChartDataPointMouseOut() {
      this.showAnnotationsTooltip = false;
    },

    /**
     * Check if the hovered data point is an annotation
     * point to show the annotation tooltip.
     */
    onChartDataPointMouseOver(params) {
      if (isDataPointAnnotation(params)) {
        const {
          event
        } = params;
        const toolTipFormatter = this.formatAnnotationsTooltipText || this.defaultAnnotationTooltipText;
        const {
          title = '',
          content = ''
        } = toolTipFormatter(params);
        this.showAnnotationsTooltip = true;
        this.annotationsTooltipTitle = title;
        this.annotationsTooltipContent = content;
        this.annotationsTooltipPosition = {
          left: `${event.event.zrX}px`,
          top: `${event.event.zrY + ANNOTATION_TOOLTIP_TOP_OFFSET}px`
        };
      }
    },

    onLabelChange(params) {
      this.selectedFormatTooltipText(params);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"position-relative"},[_c('chart',_vm._g(_vm._b({attrs:{"options":_vm.options},on:{"created":_vm.onCreated}},'chart',_vm.$attrs,false),_vm.$listeners)),_vm._v(" "),(_vm.shouldShowAnnotationsTooltip)?_c('chart-tooltip',{ref:"annotationsTooltip",attrs:{"id":"annotationsTooltip","show":_vm.showAnnotationsTooltip,"chart":_vm.chart,"top":_vm.annotationsTooltipPosition.top,"left":_vm.annotationsTooltipPosition.left,"placement":"bottom"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',[_vm._v(_vm._s(_vm.annotationsTooltipTitle))])]},proxy:true}],null,false,1889294429)},[_vm._v(" "),_c('div',[_vm._v(_vm._s(_vm.annotationsTooltipContent))])]):_vm._e(),_vm._v(" "),(_vm.chart)?_c('chart-tooltip',{ref:"dataTooltip",staticClass:"gl-pointer-events-none",attrs:{"id":"dataTooltip","show":_vm.showDataTooltip,"chart":_vm.chart,"top":_vm.dataTooltipPosition.top,"left":_vm.dataTooltipPosition.left},scopedSlots:_vm._u([{key:"title",fn:function(){return [(_vm.formatTooltipText)?_vm._t("tooltip-title"):_c('div',[_vm._v("\n        "+_vm._s(_vm.dataTooltipTitle)+"\n        "),(_vm.options.xAxis.name)?[_vm._v("("+_vm._s(_vm.options.xAxis.name)+")")]:_vm._e()],2)]},proxy:true}],null,true)},[_vm._v(" "),(_vm.formatTooltipText)?_vm._t("tooltip-content"):_c('tooltip-default-format',{attrs:{"tooltip-content":_vm.dataTooltipContent}})],2):_vm._e(),_vm._v(" "),(_vm.compiledOptions)?_c('chart-legend',{style:(_vm.legendStyle),attrs:{"chart":_vm.chart,"series-info":_vm.seriesInfo,"text-style":_vm.compiledOptions.textStyle,"min-text":_vm.legendMinText,"max-text":_vm.legendMaxText,"average-text":_vm.legendAverageText,"current-text":_vm.legendCurrentText,"layout":_vm.legendLayout}}):_vm._e()],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
