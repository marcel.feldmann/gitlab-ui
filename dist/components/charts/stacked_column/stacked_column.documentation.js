var description = "The `presentation` property allows you to change between a stacked and tiled presentation style. It\nis only setup to accept `stacked` or `tiled` as values, the default value is `tiled`.\n\nThe `stacked` presentation allows to view multiple series of the same stack as a single column,\nwhile `tiled` presents the information in multiple columns for each series of a stack.\n\n`groupBy` is a property that defines how the data is going to be grouped by for each of the series\nthat the `data` property provides. For example if the `data` property has a total of 3 series, with\n7 elements each, `groupBy` could use a 7 element array to show 7 stacked bars or 7 groups of bars\ndepending on the preference set by the `presentation` property.\n";

var stacked_column_documentation = {
  followsDesignSystem: false,
  description
};

export default stacked_column_documentation;
