import merge from 'lodash/merge';
import defaultChartOptions, { yAxis, generateBarSeries, generateLineSeries, gridWithSecondaryYAxis, grid, dataZoomAdjustments, mergeSeriesToOptions, getDefaultTooltipContent } from '../../../utils/charts/config';
import { CHART_TYPE_LINE, TOOLTIP_LEFT_OFFSET } from '../../../utils/charts/constants';
import { colorFromDefaultPalette } from '../../../utils/charts/theme';
import { columnOptions } from '../../../utils/constants';
import { debounceByAnimationFrame } from '../../../utils/utils';
import ToolboxMixin from '../../mixins/toolbox_mixin';
import TooltipDefaultFormat from '../../shared_components/charts/tooltip_default_format';
import Chart from '../chart/chart';
import ChartTooltip from '../tooltip/tooltip';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const yAxisDefaults = { ...yAxis,
  nameLocation: 'center',
  axisTick: {
    show: false
  }
};
var script = {
  components: {
    Chart,
    ChartTooltip,
    TooltipDefaultFormat
  },
  mixins: [ToolboxMixin],
  inheritAttrs: false,
  props: {
    bars: {
      type: Array,
      required: false,
      default: () => []
    },
    lines: {
      type: Array,
      required: false,
      default: () => []
    },
    secondaryData: {
      type: Array,
      required: false,
      default: () => []
    },
    option: {
      type: Object,
      required: false,
      default: () => ({})
    },
    yAxisTitle: {
      type: String,
      required: true
    },
    secondaryDataTitle: {
      type: String,
      required: false,
      default: ''
    },
    xAxisTitle: {
      type: String,
      required: true
    },
    xAxisType: {
      type: String,
      required: true,
      validator: value => ['value', 'category', 'time', 'log'].indexOf(value) !== -1
    }
  },

  data() {
    return {
      chart: null,
      showTooltip: false,
      tooltipTitle: '',
      tooltipContent: {},
      tooltipPosition: {
        left: '0',
        top: '0'
      },
      debouncedMoveShowTooltip: debounceByAnimationFrame(this.moveShowTooltip)
    };
  },

  computed: {
    hasSecondaryAxis() {
      return Boolean(this.secondaryData.length);
    },

    barSeries() {
      return this.bars.map((_ref, index) => {
        let {
          name,
          data
        } = _ref;
        const color = colorFromDefaultPalette(index);
        return generateBarSeries({
          stack: this.groupBy,
          name,
          data,
          color
        });
      });
    },

    lineSeries() {
      const offset = this.bars.length;
      return this.lines.map((_ref2, index) => {
        let {
          name,
          data
        } = _ref2;
        const color = colorFromDefaultPalette(offset + index);
        return generateLineSeries({
          name,
          data,
          color
        });
      });
    },

    secondarySeries() {
      const offset = this.bars.length + this.lines.length;
      return this.secondaryData.map((_ref3, index) => {
        let {
          name,
          data,
          type,
          stack = columnOptions.tiled
        } = _ref3;
        const color = colorFromDefaultPalette(offset + index);
        return type === CHART_TYPE_LINE ? generateLineSeries({
          color,
          name,
          data,
          yAxisIndex: 1
        }) : generateBarSeries({
          color,
          name,
          data,
          yAxisIndex: 1,
          stack
        });
      });
    },

    series() {
      return [...this.barSeries, ...this.lineSeries, ...this.secondarySeries];
    },

    options() {
      const yAxisPrimary = { ...yAxisDefaults,
        name: this.yAxisTitle
      };
      const mergedOptions = merge({}, defaultChartOptions, {
        grid: this.hasSecondaryAxis ? gridWithSecondaryYAxis : grid,
        xAxis: {
          boundaryGap: true,
          axisLabel: {
            margin: 20,
            verticalAlign: 'bottom'
          },
          axisLine: {
            show: false
          },
          axisPointer: {
            show: true,
            type: 'none',
            label: {
              formatter: this.onLabelChange
            }
          },
          name: this.xAxisTitle,
          type: this.xAxisType
        },
        yAxis: this.hasSecondaryAxis ? [yAxisPrimary, { ...yAxisDefaults,
          name: this.secondaryDataTitle,
          show: this.hasSecondaryAxis
        }] : yAxisPrimary,
        legend: {
          show: false
        }
      }, this.option, dataZoomAdjustments(this.option.dataZoom), this.toolboxAdjustments); // All chart options can be merged but series
      // needs to be handled specially

      return mergeSeriesToOptions(mergedOptions, this.series);
    }

  },

  beforeDestroy() {
    this.chart.getDom().removeEventListener('mousemove', this.debouncedMoveShowTooltip);
    this.chart.getDom().removeEventListener('mouseout', this.debouncedMoveShowTooltip);
  },

  methods: {
    moveShowTooltip(mouseEvent) {
      this.tooltipPosition = {
        left: `${mouseEvent.zrX + TOOLTIP_LEFT_OFFSET}px`,
        top: `${mouseEvent.zrY}px`
      };
      this.showTooltip = this.chart.containPixel('grid', [mouseEvent.zrX, mouseEvent.zrY]);
    },

    onCreated(chart) {
      chart.getDom().addEventListener('mousemove', this.debouncedMoveShowTooltip);
      chart.getDom().addEventListener('mouseout', this.debouncedMoveShowTooltip);
      this.chart = chart;
      this.$emit('created', chart);
    },

    onLabelChange(params) {
      const {
        xLabels,
        tooltipContent
      } = getDefaultTooltipContent(params, this.yAxisTitle);
      this.$set(this, 'tooltipContent', tooltipContent);
      this.tooltipTitle = xLabels.join(', ');
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"position-relative"},[_c('chart',_vm._g(_vm._b({attrs:{"options":_vm.options},on:{"created":_vm.onCreated}},'chart',_vm.$attrs,false),_vm.$listeners)),_vm._v(" "),(_vm.chart)?_c('chart-tooltip',{attrs:{"show":_vm.showTooltip,"chart":_vm.chart,"top":_vm.tooltipPosition.top,"left":_vm.tooltipPosition.left},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',[_vm._v(_vm._s(_vm.tooltipTitle)+" ("+_vm._s(_vm.xAxisTitle)+")")])]},proxy:true}],null,false,643259221)},[_vm._v(" "),_c('tooltip-default-format',{attrs:{"tooltip-content":_vm.tooltipContent}})],1):_vm._e()],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
