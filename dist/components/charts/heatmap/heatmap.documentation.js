var description = "## Heatmaps\n\n**Note** This component uses `<gl-legend>`, which should allow a user to click on any data point(s)\nin the legend and make the corresponding data point(s) on the chart rendered disappear.\n_See [area chart](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/charts-area-chart--default)\nfor an example_ For this particular chart, there is a [known issue](https://gitlab.com/gitlab-org/gitlab-ui/issues/352)\nwith the functionality of the legend, where clicking on it does nothing.\n";

var heatmap_documentation = {
  description,
  followsDesignSystem: true
};

export default heatmap_documentation;
