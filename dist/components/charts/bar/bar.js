import merge from 'lodash/merge';
import truncate from 'lodash/truncate';
import { dataZoomAdjustments, mergeSeriesToOptions, grid } from '../../../utils/charts/config';
import { TOOLTIP_LEFT_OFFSET } from '../../../utils/charts/constants';
import { colorFromDefaultPalette } from '../../../utils/charts/theme';
import { engineeringNotation } from '../../../utils/number_utils';
import { debounceByAnimationFrame, hexToRgba } from '../../../utils/utils';
import ToolboxMixin from '../../mixins/toolbox_mixin';
import TooltipDefaultFormat from '../../shared_components/charts/tooltip_default_format';
import Chart from '../chart/chart';
import ChartTooltip from '../tooltip/tooltip';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

/**
 * `nameGap` in charts/config is set to 50 but it is not
 * used for bar charts as the axes are flipped. That is why
 * we're explicitly setting it here
 */

const DEFAULT_NAME_GAP = 50;
/**
 * This is the magic number at which the y-axis name
 * and y-axis labels don't overlap
 * @Number
 */

const AXIS_LABEL_LENGTH = 7;
/**
 * Because the axes are reversed in bar charts defaultChartOptions
 * xAxis and yAxis needs to be handled specifically.
 */

const defaultOptions = {
  grid,
  xAxis: {
    nameLocation: 'center',
    axisLabel: {
      formatter: num => engineeringNotation(num, 2)
    }
  },
  yAxis: {
    nameGap: DEFAULT_NAME_GAP,
    boundaryGap: true,
    nameLocation: 'center',
    splitLine: {
      show: false
    },
    axisLabel: {
      interval: 0,
      formatter: str => truncate(str, {
        length: AXIS_LABEL_LENGTH,
        separator: '...'
      })
    }
  }
};
var script = {
  components: {
    Chart,
    ChartTooltip,
    TooltipDefaultFormat
  },
  mixins: [ToolboxMixin],
  inheritAttrs: false,
  props: {
    data: {
      type: Object,
      required: true
    },
    option: {
      type: Object,
      required: false,
      default: () => ({})
    },
    yAxisTitle: {
      type: String,
      required: true
    },
    xAxisTitle: {
      type: String,
      required: true
    },
    xAxisType: {
      type: String,
      required: false,
      default: 'value'
    }
  },

  data() {
    return {
      chart: null,
      showTooltip: false,
      tooltipTitle: '',
      tooltipContent: {},
      tooltipPosition: {
        left: '0',
        top: '0'
      },
      debouncedMoveShowTooltip: debounceByAnimationFrame(this.moveShowTooltip)
    };
  },

  computed: {
    series() {
      return Object.keys(this.data).map((key, index) => {
        const barColor = colorFromDefaultPalette(index);
        return {
          name: key,
          data: this.data[key],
          type: 'bar',
          stack: 'chart',
          itemStyle: {
            color: hexToRgba(barColor, 0.2),
            borderColor: barColor,
            borderWidth: 1
          },
          emphasis: {
            itemStyle: {
              color: hexToRgba(barColor, 0.4)
            }
          },
          barMaxWidth: '50%'
        };
      });
    },

    options() {
      const mergedOptions = merge({}, defaultOptions, {
        xAxis: {
          axisLine: {
            show: false
          },
          name: this.xAxisTitle,
          type: this.xAxisType
        },
        yAxis: {
          name: this.yAxisTitle,
          type: 'category',
          axisTick: {
            show: true
          },
          axisPointer: {
            show: true,
            type: 'none',
            label: {
              formatter: this.onLabelChange
            }
          }
        },
        legend: {
          show: false
        }
      }, this.option, dataZoomAdjustments(this.option.dataZoom), this.toolboxAdjustments); // All chart options can be merged but series
      // needs to be handled specially

      return mergeSeriesToOptions(mergedOptions, this.series);
    }

  },

  beforeDestroy() {
    if (this.chart) {
      this.chart.getDom().removeEventListener('mousemove', this.debouncedMoveShowTooltip);
      this.chart.getDom().removeEventListener('mouseout', this.debouncedMoveShowTooltip);
    }
  },

  methods: {
    moveShowTooltip(mouseEvent) {
      this.tooltipPosition = {
        left: `${mouseEvent.zrX + TOOLTIP_LEFT_OFFSET}px`,
        top: `${mouseEvent.zrY}px`
      };
      this.showTooltip = this.chart.containPixel('grid', [mouseEvent.zrX, mouseEvent.zrY]);
    },

    onCreated(chart) {
      this.chart = chart;
      this.$emit('created', chart);
      chart.getDom().addEventListener('mousemove', this.debouncedMoveShowTooltip);
      chart.getDom().addEventListener('mouseout', this.debouncedMoveShowTooltip);
    },

    onLabelChange(params) {
      const {
        yLabels,
        tooltipContent
      } = this.getDefaultTooltipContent(params, this.xAxisTitle);
      this.$set(this, 'tooltipContent', tooltipContent);
      this.tooltipTitle = yLabels.join(', ');
    },

    /**
     * The existing getDefaultTooltipContent in utils works against the y-axis value.
     * However, for bar charts, the tooltip should be against x-axis values.
     * This method will be removed after https://gitlab.com/gitlab-org/gitlab-ui/-/issues/674
     *
     * @param {Object} params series data
     * @param {String} xAxisTitle x-axis title
     * @returns {Object} tooltip title and content
     */
    getDefaultTooltipContent(params) {
      let xAxisTitle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      const seriesDataLength = params.seriesData.length;
      const {
        yLabels,
        tooltipContent
      } = params.seriesData.reduce((acc, chartItem) => {
        const [value, title] = chartItem.value || []; // The x axis title is used instead of y axis

        const seriesName = seriesDataLength === 1 && xAxisTitle ? xAxisTitle : chartItem.seriesName;
        const color = seriesDataLength === 1 ? '' : chartItem.color;
        acc.tooltipContent[seriesName] = {
          value,
          color
        };

        if (!acc.yLabels.includes(title)) {
          acc.yLabels.push(title);
        }

        return acc;
      }, {
        yLabels: [],
        tooltipContent: {}
      });
      return {
        yLabels,
        tooltipContent
      };
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"position-relative"},[_c('chart',_vm._g(_vm._b({attrs:{"options":_vm.options},on:{"created":_vm.onCreated}},'chart',_vm.$attrs,false),_vm.$listeners)),_vm._v(" "),(_vm.chart)?_c('chart-tooltip',{attrs:{"show":_vm.showTooltip,"chart":_vm.chart,"top":_vm.tooltipPosition.top,"left":_vm.tooltipPosition.left},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',[_vm._v(_vm._s(_vm.tooltipTitle)+" ("+_vm._s(_vm.yAxisTitle)+")")])]},proxy:true}],null,false,1644826356)},[_vm._v(" "),_c('tooltip-default-format',{attrs:{"tooltip-content":_vm.tooltipContent}})],1):_vm._e()],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
