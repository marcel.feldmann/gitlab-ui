var description = "A bar chart is similar to a column chart where the the length of bars represents the data value.\nAlthough alike, they are cannot be interchangeably used. Bar charts are good for displaying large\nnumber of categories on the y-axis.\n";

var bar_documentation = {
  followsDesignSystem: true,
  description
};

export default bar_documentation;
