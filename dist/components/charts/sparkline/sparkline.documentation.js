var description = "\nThis is a basic sparkline chart.\n\n### Implementation Details\n\nThis component wraps the Gitlab UI `chart` component, which in turn wraps the ECharts component.\n\nSee the [chart](./?path=/story/charts-chart--default) component for more info.\n";

var sparkline_documentation = {
  followsDesignSystem: false,
  description
};

export default sparkline_documentation;
