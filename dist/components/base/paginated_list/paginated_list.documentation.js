var description = "The paginated list component allows the easy creation of list with pagination and client side sorting.\n";

var paginated_list_documentation = {
  description
};

export default paginated_list_documentation;
