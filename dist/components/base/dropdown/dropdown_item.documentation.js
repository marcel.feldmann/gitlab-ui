var description = "The dropdown item component is meant to be used for clickable entries inside a dropdown component.\nIf you provide the `href` attribute, it renders a link instead of a button.\n";

var dropdown_item_documentation = {
  description
};

export default dropdown_item_documentation;
