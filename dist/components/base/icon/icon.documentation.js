var description = "## Usage\n\nThe Icon component can be used to render any svg within the @gitlab/svgs icon sprites.\n\n### Accessibility\n\n`gl-icon` icons are hidden from screen readers by default, as usages of icons are commonly decorative.\n\nIf the icon is not decorative, add an `aria-label` attribute to `gl-icon` to give it an accessible name.\nThis label is read out by screen readers.\n\nIf the icon is clickable, use `gl-button` instead of `gl-icon` because a clickable element should\nsemantically be a button.\n\n```html\n<!-- icon, which is hidden from screen readers by default  -->\n<gl-icon name=\"rocket\" />\n\n<!-- icon, which has an accessible name \"Confidential issue\" that is read out by screen readers -->\n<gl-icon name=\"eye-slash\" :aria-label=\"__('Confidential issue')\" />\n\n<!-- clickable icon, which is borderless and padding-less -->\n<gl-button icon=\"close\" category=\"tertiary\" class=\"gl-p-0!\" aria-label=\"Close\" />\n```\n\nFor more information about icons within GitLab, visit the\n[GitLab accessibility guidelines](https://docs.gitlab.com/ee/development/fe_guide/accessibility#icons).\n";

var icon_documentation = {
  followsDesignSystem: true,
  description
};

export default icon_documentation;
