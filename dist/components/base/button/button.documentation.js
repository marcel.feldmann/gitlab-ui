var description = "Buttons execute an action, either in the background or foreground of an experience. Different button\ncategories help guide users through certain actions. Buttons express what action will occur when the\nuser clicks or touches it either by text, icon, or both. Additional meaning can be derived from the\nbutton variant.\n\n## Button link\n\nA button link is a link that is styled to look like a button, semantically speaking it's a `<a>` tag\nwith the necessary classes added to make it look like a button, it shares the same functionality as\n[`<gl-link>`]\n\n> Note: Setting a `target` attribute without a `href` attribute, will not create any side effects.\n> Without the presence of a `href` attribute, this component will render a `<button>`.\n\n[`<gl-link>`]: ./?path=/story/base-link--default-link\n\n## Icon-only button\n\nIcon-only buttons must have an accessible name.\nYou can provide one with the `aria-label` attribute, which is read out by screen readers.\n\n```html\n<gl-button icon=\"close\" aria-label=\"Close\" />\n```\n\n## Label button\n\nA label button renders a non-interactive `span` styled as a button. This can be especially useful\nwhen used in a button group to render text-only labels along with actionable buttons. To improve\naccessibility, and when applicable, consider using [`aria-describedby`] to establish a\nrelationship between the label button and the associated button.\n\n[`aria-describedby`]: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-describedby_attribute\n\n## Security\n\nThis component implements a few security measures to make it as safe as possible by default.\nSee [SafeLinkDirective docs] for more details.\n\n### Linking to an unsafe URL\n\nIf you're trying to link to a location considered unsafe by the `SafeLink` directive (when rendering\na download link with a [Data URL] for example), you'll need to bypass the `href` attribute's\nsanitization. This component exposes the `is-unsafe-link` prop for that purpose.\n\n> **Warning:** Only disable URL sanitization when absolutely necessary.\n\n```html\n<gl-button\n  is-unsafe-link\n  download=\"file.txt\"\n  href=\"data:text/plain;charset=utf-8,GitLab%20is%20awesome\">Download</gl-button>\n```\n\n[SafeLinkDirective docs]: https://gitlab-org.gitlab.io/gitlab-ui/?path=/docs/directives-safe-link-directive--default\n[Data URL]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs\n\n## vue-bootstrap component\n\nThis component uses [`BButton`](https://bootstrap-vue.org/docs/components/button) from vue-bootstrap\ninternally. So please take a look also there at their extensive documentation.\n";

var button_documentation = {
  description,
  followsDesignSystem: true,
  bootstrapComponent: 'b-button',
  bootstrapPropsInfo: {
    category: {
      enum: 'buttonCategoryOptions'
    },
    variant: {
      enum: 'buttonVariantOptions'
    },
    size: {
      enum: 'buttonSizeOptions'
    }
  },
  events: [{
    event: 'click',
    description: 'Emitted when clicked on button'
  }]
};

export default button_documentation;
