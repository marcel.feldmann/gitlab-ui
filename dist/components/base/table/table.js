import { BTable } from 'bootstrap-vue/esm/index.js';
import { isDev, logWarning } from '../../../utils/utils';
import { tableFullProps, tableFullSlots, glTableLiteWarning } from './constants';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const shouldUseFullTable = _ref => {
  let {
    $attrs,
    $scopedSlots
  } = _ref;
  return tableFullProps.some(prop => $attrs[prop] !== undefined) || tableFullSlots.some(slot => $scopedSlots[slot] !== undefined);
};

var script = {
  components: {
    BTable
  },
  inheritAttrs: false,

  mounted() {
    // logWarning will call isDev before logging any message
    // this additional call to isDev is being made to exit the condition early when run in production
    if (isDev() && !shouldUseFullTable(this)) {
      logWarning(glTableLiteWarning);
    }
  }

};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('b-table',_vm._g(_vm._b({staticClass:"gl-table",scopedSlots:_vm._u([_vm._l((Object.keys(_vm.$scopedSlots)),function(slot){return {key:slot,fn:function(scope){return [_vm._t(slot,null,null,scope)]}}})],null,true)},'b-table',_vm.$attrs,false),_vm.$listeners))};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
