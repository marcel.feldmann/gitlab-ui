var description = "Button groups are an easy way to group a series of buttons together.\n\n## vue-bootstrap component\n\nThis component uses [`BButtonGroup`](https://bootstrap-vue.org/docs/components/button-group) from vue-bootstrap\ninternally. So please take a look also there at their extensive documentation.\n";

var button_group_documentation = {
  description,
  followsDesignSystem: true
};

export default button_group_documentation;
