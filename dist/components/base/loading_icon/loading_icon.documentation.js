var description = "## Under the hood\n\nLoading icon uses pure css to render a spinner.\n";

var loading_icon_documentation = {
  followsDesignSystem: true,
  description
};

export default loading_icon_documentation;
