var description = "Broadcast messages provide an efficient and prominent way to deliver critical messages at the\ninstance level to all users. For example, a broadcast message can be used when an admin wants to\nannounce that their platform will experience downtime during a specific period.\n\nIn comparison with an alert, broadcast messages are created by an admin and not triggered by the\nsystem.\n\nPlease refer to [Pajamas' documentation](https://design.gitlab.com/components/broadcast-message)\nfor more information on when to use this component.\n\n## Dismiss a broadcast message\n\nThe `GlBroadcastMessage` component deals with users dismissal the same way `GlAlert` does, meaning\nit does not handle its own visibility but emits a `dismiss` event that the parent component should\nlisten to in order to hide the message. Example:\n\n```html\n<template>\n  ...\n  <gl-broadcast-message v-if=\"!dismissed\" @dismiss=\"dismissed = true\">\n    An important message\n  </gl-broadcast-message>\n  ...\n</template>\n```\n";

var broadcast_message_documentation = {
  followsDesignSystem: false,
  description
};

export default broadcast_message_documentation;
