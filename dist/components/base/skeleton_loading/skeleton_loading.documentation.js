var description = "The Skeleton Loading component has been deprecated, please use [Skeleton Loader](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-skeleton-loader--default).\n\nHow to replace this component with [Skeleton Loader](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-skeleton-loader--default):\n\n1. Update imports\n   - `import { GlSkeletonLoading } from '@gitlab/ui'` -> `import { GlSkeletonLoader } from '@gitlab/ui'`\n2. Update component definitions\n   - `components: { GlSkeletonLoading }` -> `components: { GlSkeletonLoader }`\n3. Update template\n   - `<gl-skeleton-loading />` -> `<gl-skeleton-loader />`\n";

var skeleton_loading_documentation = {
  description
};

export default skeleton_loading_documentation;
