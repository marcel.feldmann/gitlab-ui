import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  props: {
    /**
     * Controls the number of lines.
     */
    lines: {
      type: Number,
      required: false,
      default: 3,

      validator(value) {
        return value > 0 && value < 4;
      }

    }
  },
  computed: {
    lineClasses() {
      return new Array(this.lines).fill().map((_, i) => `skeleton-line-${i + 1}`);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"animation-container"},_vm._l((_vm.lineClasses),function(css,index){return _c('div',{key:index,class:css})}),0)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
