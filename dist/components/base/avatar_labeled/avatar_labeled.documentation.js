var description = "## Usage\n\nAvatars may also be adjacent to a text alternative, such as a user or project name. In these cases,\na null `alt` text should be used so that they can be ignored by assistive technologies.\n\nUse the `avatar-labeled` component in those scenarios. It will set a null `alt` text by default.\nIt allows to display a `label` and/or a `sub-label` next to the avatar image. It accepts the same\nproperties as the avatar component to modify the avatar’s shape and size.\n\n## Using the component\n\n```js\n<gl-avatar-labeled\n  :shape=\"shape\"\n  :size=\"size\"\n  :src=\"src\"\n  :label=\"label\"\n  :sub-label=\"subLabel\"\n/>\n```\n";

var avatar_labeled_documentation = {
  followsDesignSystem: true,
  description
};

export default avatar_labeled_documentation;
