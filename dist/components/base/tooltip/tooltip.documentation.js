var description = "Using the tooltip component is recommended if you have HTML content.\nIt is also currently required if the tooltip content needs to change while it's visible\n(see [this upstream issue][this upstream issue]). In all other cases, please use the directive.\n\n[this upstream issue]: https://github.com/bootstrap-vue/bootstrap-vue/issues/2142\n\n## Using the component\n\n~~~html\n<gl-button ref=\"someButton\">\n  ...\n</gl-button>\n\n<gl-tooltip :target=\"() => $refs.someButton\">\n  some <em>tooltip<em/> text\n</gl-tooltip>\n~~~\n\n## Using the directive\n\nYou will need to import and register `GlTooltipDirective` before you can use it.\n\n~~~html\n<script>\nimport { GlTooltipDirective } from '@gitlab/ui';\n\nexport default {\n  directives: {\n    GlTooltip: GlTooltipDirective,\n  },\n};\n</script>\n\n<element\n  v-gl-tooltip.${modifier}\n  title=\"some tooltip text\"\n>\n  ...\n</element>\n~~~\n\n## Directive attributes\n\n`v-gl-tooltip` directive uses the same attributes as [`v-b-tooltip`][`v-b-tooltip`].\n\n## Under the hood\n\nTooltip uses [`<b-tooltip>`][`<b-tooltip>`] and [`v-b-tooltip`][`v-b-tooltip`] internally.\n\n[`<b-tooltip>`]: https://bootstrap-vue.org/docs/components/tooltip\n\n[`v-b-tooltip`]: https://bootstrap-vue.org/docs/directives/tooltip\n";

var tooltip_documentation = {
  followsDesignSystem: true,
  description
};

export default tooltip_documentation;
