var description = "## Usage\n\nPaths are horizontal process flows composed of a series of \"stages\".\nLike tabs, paths let users focus in on specific content at each stage\nwhilst still keeping all the other stages in view. Only one stage can\nbe active at a given time.\n\n### Implemetation\n\nThe component should be initialized with an array of data objects. By\ndefault, the first item in the array will be selected. This can be\noverridden by passing in an object with the selected property set to\ntrue.\n\n```js\nitems: [\n  {\n    title: 'First',\n  },\n  {\n    title: 'Second',\n    selected: true\n  },\n  ...\n```\n\nOnce an item has been selected the `selected` event will be emitted.\nThe emitted event will include the entire object at the selected index.\n\n#### Customization\n\nAdditional attributes can be configured via the `items` object. Currently\nsupport for `metric` and `icon` are provided. Please see the individual\nexamples for further information on these.\n\n### Additional information\n\nA `backgroundColor` property can be specified when using this component\non different colored backgrounds.\n\nThis component supports various themes and is mobile responsive.\n";

var path_documentation = {
  description,
  followsDesignSystem: true
};

export default path_documentation;
