var token_selector = "Choose from a provided list of tokens or add a user defined token.\n\n```html\n<script>\nexport default {\n  data() {\n    return {\n      selectedTokens: [\n        {\n          id: 1,\n          name: 'Vue.js',\n        },\n      ],\n    };\n  },\n};\n</script>\n\n<template>\n  <div>\n    <gl-token-selector\n      v-model=\"selectedTokens\"\n      :dropdown-items=\"[\n        {\n          id: 1,\n          name: 'Vue.js',\n        },\n        {\n          id: 2,\n          name: 'Ruby On Rails',\n        },\n        {\n          id: 3,\n          name: 'GraphQL',\n        },\n        {\n          id: 4,\n          name: 'Redis',\n        },\n      ]\"\n    />\n    {{ selectedTokens }}\n  </div>\n</template>\n```\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': token_selector
});

var token_selector_documentation = {
  description
};

export default token_selector_documentation;
