import { COMMA } from '../../../utils/constants';
import GlToken from '../token/token';
import GlFilteredSearchTokenSegment from './filtered_search_token_segment';
import { TERM_TOKEN_TYPE } from './filtered_search_utils';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const SEGMENT_TITLE = 'TYPE';
const SEGMENT_OPERATOR = 'OPERATOR';
const SEGMENT_DATA = 'DATA';
const TOKEN_CLOSE_SELECTOR = '.gl-token-close';
const DEFAULT_OPERATORS = [{
  value: '=',
  description: 'is',
  default: 'true'
}, {
  value: '!=',
  description: 'is not'
}];
var script = {
  name: 'GlFilteredSearchToken',
  components: {
    GlToken,
    GlFilteredSearchTokenSegment
  },
  inheritAttrs: false,
  props: {
    availableTokens: {
      type: Array,
      required: false,
      default: () => []
    },

    /**
     * Token configuration with available operators and options.
     */
    config: {
      type: Object,
      required: false,
      default: () => ({})
    },

    /**
     * Determines if the token is being edited or not.
     */
    active: {
      type: Boolean,
      required: false,
      default: false
    },
    multiSelectValues: {
      type: Array,
      required: false,
      default: () => []
    },

    /**
     * Current token value.
     */
    value: {
      type: Object,
      required: false,
      default: () => ({
        operator: '',
        data: ''
      })
    },

    /**
     * Display operators' descriptions instead of their values (e.g., "is" instead of "=").
     */
    showFriendlyText: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  data() {
    return {
      activeSegment: null
    };
  },

  computed: {
    operators() {
      return this.config.operators || DEFAULT_OPERATORS;
    },

    hasDataOrDataSegmentIsCurrentlyActive() {
      return this.value.data !== '' || this.isSegmentActive(SEGMENT_DATA);
    },

    availableTokensWithSelf() {
      return [this.config, ...this.availableTokens.filter(t => t !== this.config)].map(t => ({ ...t,
        value: t.title
      }));
    },

    operatorDescription() {
      const operator = this.operators.find(op => op.value === this.value.operator);
      return this.showFriendlyText ? operator === null || operator === void 0 ? void 0 : operator.description : operator === null || operator === void 0 ? void 0 : operator.value;
    }

  },
  segments: {
    SEGMENT_TITLE,
    SEGMENT_DATA,
    SEGMENT_OPERATOR
  },
  watch: {
    value: {
      deep: true,

      handler(newValue) {
        /**
         * Emitted when the token changes its value.
         *
         * @event input
         * @type {object} dataObj Object containing the update value.
         */
        this.$emit('input', newValue);
      }

    },
    active: {
      immediate: true,

      handler(newValue) {
        if (newValue) {
          if (!this.activeSegment) {
            this.activateSegment(this.value.data !== '' ? SEGMENT_DATA : SEGMENT_OPERATOR);
          }
        } else if (this.value.data === '') {
          this.activeSegment = null;
          /**
           * Emitted when token is about to be destroyed.
           *
           * @event destroy
           */

          this.$emit('destroy');
        }
      }

    }
  },

  created() {
    if (!('operator' in this.value)) {
      if (this.operators.length === 1) {
        const operator = this.operators[0].value;
        this.$emit('input', { ...this.value,
          operator
        });
        this.activeSegment = SEGMENT_DATA;
      } else {
        this.$emit('input', { ...this.value,
          operator: ''
        });
      }
    }
  },

  methods: {
    activateSegment(segment) {
      this.activeSegment = segment;

      if (!this.active) {
        /**
         * Emitted when this term token is clicked.
         *
         * @event activate
         */
        this.$emit('activate');
      }
    },

    getAdditionalSegmentClasses(segment) {
      return {
        'gl-cursor-pointer': !this.isSegmentActive(segment)
      };
    },

    isSegmentActive(segment) {
      return this.active && this.activeSegment === segment;
    },

    replaceWithTermIfEmpty() {
      if (this.value.operator === '' && this.value.data === '') {
        /**
         * Emitted when this token is converted to another type
         * @property {object} token Replacement token configuration
         */
        this.$emit('replace', {
          type: TERM_TOKEN_TYPE,
          value: {
            data: this.config.title
          }
        });
      }
    },

    replaceToken(newTitle) {
      const newTokenConfig = this.availableTokens.find(t => t.title === newTitle);

      if (newTokenConfig === this.config) {
        this.$nextTick(() => {
          /**
           * Emitted when this term token will lose its focus.
           *
           * @event deactivate
           */
          this.$emit('deactivate');
        });
        return;
      }

      if (newTokenConfig) {
        const isCompatible = this.config.dataType && this.config.dataType === newTokenConfig.dataType;
        this.$emit('replace', {
          type: newTokenConfig.type,
          value: isCompatible ? this.value : {
            data: ''
          }
        });
      }
    },

    handleOperatorKeydown(evt, _ref) {
      let {
        inputValue,
        suggestedValue,
        applySuggestion
      } = _ref;
      const {
        key
      } = evt;

      if (key === ' ' || key === 'Spacebar') {
        applySuggestion(suggestedValue);
        return;
      }

      const potentialValue = `${inputValue}${key}`;

      if (key.length === 1 && !this.operators.find(_ref2 => {
        let {
          value
        } = _ref2;
        return value.startsWith(potentialValue);
      })) {
        if (this.value.data === '') {
          applySuggestion(suggestedValue);
        } else {
          evt.preventDefault();
        }
      }
    },

    activateDataSegment() {
      if (this.config.multiSelect) {
        this.$emit('input', { ...this.value,
          data: ''
        });
      }

      this.activateSegment(this.$options.segments.SEGMENT_DATA);
    },

    handleComplete() {
      if (this.config.multiSelect) {
        this.$emit('input', { ...this.value,
          data: this.multiSelectValues.join(COMMA)
        });
      }
      /**
       * Emitted when the token entry has been completed.
       *
       * @event complete
       */


      this.$emit('complete');
    },

    destroyByClose(event) {
      if (event.target.closest(TOKEN_CLOSE_SELECTOR)) {
        event.preventDefault();
        this.$emit('destroy');
      }
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"gl-filtered-search-token",class:{ 'gl-filtered-search-token-active': _vm.active }},[_c('gl-filtered-search-token-segment',{key:"title-segment",attrs:{"value":_vm.config.title,"active":_vm.isSegmentActive(_vm.$options.segments.SEGMENT_TITLE),"options":_vm.availableTokensWithSelf},on:{"activate":function($event){return _vm.activateSegment(_vm.$options.segments.SEGMENT_TITLE)},"deactivate":function($event){return _vm.$emit('deactivate')},"complete":_vm.replaceToken,"backspace":function($event){return _vm.$emit('destroy')},"submit":function($event){return _vm.$emit('submit')}},scopedSlots:_vm._u([{key:"view",fn:function(ref){
var inputValue = ref.inputValue;
return [_c('gl-token',{staticClass:"gl-filtered-search-token-type",class:_vm.getAdditionalSegmentClasses(_vm.$options.segments.SEGMENT_TITLE),attrs:{"view-only":""}},[_vm._v(_vm._s(inputValue))])]}}])}),_vm._v(" "),_c('gl-filtered-search-token-segment',{key:"operator-segment",attrs:{"active":_vm.isSegmentActive(_vm.$options.segments.SEGMENT_OPERATOR),"options":_vm.operators,"custom-input-keydown-handler":_vm.handleOperatorKeydown,"view-only":""},on:{"activate":function($event){return _vm.activateSegment(_vm.$options.segments.SEGMENT_OPERATOR)},"backspace":_vm.replaceWithTermIfEmpty,"complete":function($event){return _vm.activateSegment(_vm.$options.segments.SEGMENT_DATA)},"deactivate":function($event){return _vm.$emit('deactivate')}},scopedSlots:_vm._u([{key:"view",fn:function(){return [_c('gl-token',{staticClass:"gl-filtered-search-token-operator",class:_vm.getAdditionalSegmentClasses(_vm.$options.segments.SEGMENT_OPERATOR),attrs:{"variant":"search-value","view-only":""}},[_vm._v(_vm._s(_vm.operatorDescription))])]},proxy:true},{key:"option",fn:function(ref){
var option = ref.option;
return [_c('div',{staticClass:"gl-display-flex"},[_vm._v("\n        "+_vm._s(option.value)+"\n        "),(option.description)?_c('span',{staticClass:"gl-filtered-search-token-operator-description"},[_vm._v("\n          "+_vm._s(option.description)+"\n        ")]):_vm._e()])]}}]),model:{value:(_vm.value.operator),callback:function ($$v) {_vm.$set(_vm.value, "operator", $$v);},expression:"value.operator"}}),_vm._v(" "),(_vm.hasDataOrDataSegmentIsCurrentlyActive)?_c('gl-filtered-search-token-segment',{key:"data-segment",attrs:{"active":_vm.isSegmentActive(_vm.$options.segments.SEGMENT_DATA),"multi-select":_vm.config.multiSelect,"options":_vm.config.options,"option-text-field":"title"},on:{"activate":_vm.activateDataSegment,"backspace":function($event){return _vm.activateSegment(_vm.$options.segments.SEGMENT_OPERATOR)},"complete":_vm.handleComplete,"select":function($event){return _vm.$emit('select', $event)},"submit":function($event){return _vm.$emit('submit')},"deactivate":function($event){return _vm.$emit('deactivate')},"split":function($event){return _vm.$emit('split', $event)}},scopedSlots:_vm._u([{key:"suggestions",fn:function(){return [_vm._t("suggestions")]},proxy:true},{key:"view",fn:function(ref){
var inputValue = ref.inputValue;
return [_vm._t("view-token",[_c('gl-token',{staticClass:"gl-filtered-search-token-data",class:_vm.getAdditionalSegmentClasses(_vm.$options.segments.SEGMENT_DATA),attrs:{"variant":"search-value"},on:{"mousedown":_vm.destroyByClose}},[_c('span',{staticClass:"gl-filtered-search-token-data-content"},[_vm._t("view",[_vm._v(_vm._s(inputValue))],null,{ inputValue: inputValue })],2)])],null,{
          inputValue: inputValue,
          listeners: { mousedown: _vm.destroyByClose },
          cssClasses: Object.assign({}, {'gl-filtered-search-token-data': true},
            _vm.getAdditionalSegmentClasses(_vm.$options.segments.SEGMENT_DATA)),
        })]}}],null,true),model:{value:(_vm.value.data),callback:function ($$v) {_vm.$set(_vm.value, "data", $$v);},expression:"value.data"}}):_vm._e()],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
