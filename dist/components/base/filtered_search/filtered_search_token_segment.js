import _last from 'lodash/last';
import { Portal } from 'portal-vue';
import { COMMA } from '../../../utils/constants';
import GlFilteredSearchSuggestion from './filtered_search_suggestion';
import GlFilteredSearchSuggestionList from './filtered_search_suggestion_list';
import { splitOnQuotes, wrapTokenInQuotes } from './filtered_search_utils';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  name: 'GlFilteredSearchTokenSegment',
  components: {
    Portal,
    GlFilteredSearchSuggestionList,
    GlFilteredSearchSuggestion
  },
  inject: ['portalName', 'alignSuggestions'],
  inheritAttrs: false,
  props: {
    /**
     * If this token segment is currently being edited.
     */
    active: {
      type: Boolean,
      required: false,
      default: false
    },
    label: {
      type: String,
      required: false,
      default: 'Search'
    },
    multiSelect: {
      type: Boolean,
      required: false,
      default: false
    },
    options: {
      type: Array,
      required: false,
      default: () => null
    },
    optionTextField: {
      type: String,
      required: false,
      default: 'value'
    },
    customInputKeydownHandler: {
      type: Function,
      required: false,
      default: () => () => false
    },

    /**
     * Current term value
     */
    value: {
      required: true,
      validator: () => true
    },

    /**
     * HTML attributes to add to the search input
     */
    searchInputAttributes: {
      type: Object,
      required: false,
      default: () => ({})
    },

    /**
     * If this is the last token
     */
    isLastToken: {
      type: Boolean,
      required: false,
      default: false
    },
    currentValue: {
      type: Array,
      required: false,
      default: () => []
    }
  },

  data() {
    return {
      fallbackValue: this.value
    };
  },

  computed: {
    matchingOption() {
      var _this$options;

      return (_this$options = this.options) === null || _this$options === void 0 ? void 0 : _this$options.find(o => o.value === this.value);
    },

    nonMultipleValue() {
      return this.multiSelect ? _last(this.value.split(COMMA)) : this.value;
    },

    inputValue: {
      get() {
        return this.matchingOption ? this.matchingOption[this.optionTextField] : this.nonMultipleValue;
      },

      set(v) {
        var _this$getMatchingOpti, _this$getMatchingOpti2;

        /**
         * Emitted when this token segment's value changes.
         *
         * @type {object} option The current option.
         */
        this.$emit('input', (_this$getMatchingOpti = (_this$getMatchingOpti2 = this.getMatchingOptionForInputValue(v)) === null || _this$getMatchingOpti2 === void 0 ? void 0 : _this$getMatchingOpti2.value) !== null && _this$getMatchingOpti !== void 0 ? _this$getMatchingOpti : v);
      }

    },

    hasOptionsOrSuggestions() {
      var _this$options2;

      return ((_this$options2 = this.options) === null || _this$options2 === void 0 ? void 0 : _this$options2.length) || this.$slots.suggestions;
    },

    defaultSuggestedValue() {
      var _ref;

      if (!this.options) {
        return this.nonMultipleValue;
      }

      if (this.value) {
        const match = this.getMatchingOptionForInputValue(this.inputValue) || this.getMatchingOptionForInputValue(this.inputValue, {
          loose: true
        });
        return match === null || match === void 0 ? void 0 : match.value;
      }

      const defaultSuggestion = this.options.find(op => op.default);
      return (_ref = defaultSuggestion !== null && defaultSuggestion !== void 0 ? defaultSuggestion : this.options[0]) === null || _ref === void 0 ? void 0 : _ref.value;
    },

    containerAttributes() {
      return this.isLastToken && !this.active && this.currentValue.length > 1 && this.searchInputAttributes;
    }

  },
  watch: {
    active: {
      immediate: true,

      handler(newValue) {
        if (newValue) {
          this.activate();
        } else {
          this.deactivate();
        }
      }

    },

    inputValue(newValue) {
      var _this$getMatchingOpti3, _this$getMatchingOpti4;

      const hasUnclosedQuote = newValue.split('"').length % 2 === 0;

      if (newValue.indexOf(' ') === -1 || hasUnclosedQuote) {
        return;
      }

      const [firstWord, ...otherWords] = splitOnQuotes(newValue).filter((w, idx, arr) => Boolean(w) || idx === arr.length - 1);
      this.$emit('input', (_this$getMatchingOpti3 = (_this$getMatchingOpti4 = this.getMatchingOptionForInputValue(firstWord)) === null || _this$getMatchingOpti4 === void 0 ? void 0 : _this$getMatchingOpti4.value) !== null && _this$getMatchingOpti3 !== void 0 ? _this$getMatchingOpti3 : firstWord);

      if (otherWords.length) {
        /**
         * Emitted when Space appears in token segment value
         * @property {array|string} newStrings New strings to be converted into term tokens
         */
        this.$emit('split', otherWords);
      }
    }

  },
  methods: {
    emitIfInactive(e) {
      if (!this.active) {
        /**
         * Emitted on mousedown event on the main component.
         */
        this.$emit('activate');
        e.preventDefault();
      }
    },

    getMatchingOptionForInputValue(v) {
      var _this$options3;

      let {
        loose
      } = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
        loose: false
      };
      return (_this$options3 = this.options) === null || _this$options3 === void 0 ? void 0 : _this$options3.find(o => loose ? o[this.optionTextField].startsWith(v) : [this.optionTextField] === v);
    },

    activate() {
      this.fallbackValue = this.value;
      this.$nextTick(() => {
        const {
          input
        } = this.$refs;

        if (input) {
          input.focus();
          input.scrollIntoView({
            block: 'nearest',
            inline: 'end'
          });
          this.alignSuggestions(input);
        }
      });
    },

    deactivate() {
      var _this$matchingOption;

      if (!this.options) {
        return;
      }

      if (((_this$matchingOption = this.matchingOption) === null || _this$matchingOption === void 0 ? void 0 : _this$matchingOption.value) !== this.value) {
        this.$emit('input', this.fallbackValue);
      }
    },

    applySuggestion(suggestedValue) {
      const formattedSuggestedValue = wrapTokenInQuotes(suggestedValue);
      /**
       * Emitted when autocomplete entry is selected.
       *
       * @type {string} value The selected value.
       */

      this.$emit('select', formattedSuggestedValue);

      if (!this.multiSelect) {
        this.$emit('input', formattedSuggestedValue);
        this.$emit('complete', formattedSuggestedValue);
      }
    },

    handleInputKeydown(e) {
      const {
        key
      } = e;
      const {
        suggestions
      } = this.$refs;
      const suggestedValue = suggestions === null || suggestions === void 0 ? void 0 : suggestions.getValue();

      if (key === 'Backspace') {
        if (this.inputValue === '') {
          e.preventDefault();
          /**
           * Emitted when Backspace is pressed and the value is empty
           */

          this.$emit('backspace');
        }

        return;
      }

      const handlers = {
        Enter: () => {
          e.preventDefault();

          if (suggestedValue != null) {
            this.applySuggestion(suggestedValue);
          } else {
            /**
             * Emitted when Enter is pressed and no suggestion is selected
             */
            this.$emit('submit');
          }
        },
        ':': () => {
          if (suggestedValue != null) {
            e.preventDefault();
            this.applySuggestion(suggestedValue);
          }
        },
        Escape: () => {
          e.preventDefault();
          /**
           * Emitted when suggestion is selected from the suggestion list
           */

          this.$emit('complete');
        }
      };
      const suggestionsHandlers = {
        ArrowDown: () => suggestions.nextItem(),
        Down: () => suggestions.nextItem(),
        ArrowUp: () => suggestions.prevItem(),
        Up: () => suggestions.prevItem()
      };

      if (this.hasOptionsOrSuggestions) {
        Object.assign(handlers, suggestionsHandlers);
      }

      if (Object.keys(handlers).includes(key)) {
        handlers[key]();
        return;
      }

      this.customInputKeydownHandler(e, {
        suggestedValue,
        inputValue: this.inputValue,
        applySuggestion: v => this.applySuggestion(v)
      });
    },

    handleBlur() {
      if (this.multiSelect) {
        this.$emit('complete');
      } else if (this.active) {
        /**
         * Emitted when this term token will lose its focus.
         */
        this.$emit('deactivate');
      }
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',_vm._b({staticClass:"gl-filtered-search-token-segment",class:{ 'gl-filtered-search-token-segment-active': _vm.active },attrs:{"data-testid":"filtered-search-token-segment"},on:{"mousedown":function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"left",37,$event.key,["Left","ArrowLeft"])){ return null; }if('button' in $event && $event.button !== 0){ return null; }return _vm.emitIfInactive($event)}}},'div',_vm.containerAttributes,false),[(_vm.active)?[(((_vm.searchInputAttributes).type)==='checkbox')?_c('input',_vm._b({directives:[{name:"model",rawName:"v-model",value:(_vm.inputValue),expression:"inputValue"}],ref:"input",staticClass:"gl-filtered-search-token-segment-input",attrs:{"aria-label":_vm.label,"type":"checkbox"},domProps:{"checked":Array.isArray(_vm.inputValue)?_vm._i(_vm.inputValue,null)>-1:(_vm.inputValue)},on:{"keydown":_vm.handleInputKeydown,"blur":_vm.handleBlur,"change":function($event){var $$a=_vm.inputValue,$$el=$event.target,$$c=$$el.checked?(true):(false);if(Array.isArray($$a)){var $$v=null,$$i=_vm._i($$a,$$v);if($$el.checked){$$i<0&&(_vm.inputValue=$$a.concat([$$v]));}else {$$i>-1&&(_vm.inputValue=$$a.slice(0,$$i).concat($$a.slice($$i+1)));}}else {_vm.inputValue=$$c;}}}},'input',_vm.searchInputAttributes,false)):(((_vm.searchInputAttributes).type)==='radio')?_c('input',_vm._b({directives:[{name:"model",rawName:"v-model",value:(_vm.inputValue),expression:"inputValue"}],ref:"input",staticClass:"gl-filtered-search-token-segment-input",attrs:{"aria-label":_vm.label,"type":"radio"},domProps:{"checked":_vm._q(_vm.inputValue,null)},on:{"keydown":_vm.handleInputKeydown,"blur":_vm.handleBlur,"change":function($event){_vm.inputValue=null;}}},'input',_vm.searchInputAttributes,false)):_c('input',_vm._b({directives:[{name:"model",rawName:"v-model",value:(_vm.inputValue),expression:"inputValue"}],ref:"input",staticClass:"gl-filtered-search-token-segment-input",attrs:{"aria-label":_vm.label,"type":(_vm.searchInputAttributes).type},domProps:{"value":(_vm.inputValue)},on:{"keydown":_vm.handleInputKeydown,"blur":_vm.handleBlur,"input":function($event){if($event.target.composing){ return; }_vm.inputValue=$event.target.value;}}},'input',_vm.searchInputAttributes,false)),_vm._v(" "),_c('portal',{key:("operator-" + _vm._uid),attrs:{"to":_vm.portalName}},[(_vm.hasOptionsOrSuggestions)?_c('gl-filtered-search-suggestion-list',{key:("operator-" + _vm._uid),ref:"suggestions",attrs:{"initial-value":_vm.defaultSuggestedValue},on:{"suggestion":_vm.applySuggestion}},[(_vm.options)?_vm._l((_vm.options),function(option,idx){return _c('gl-filtered-search-suggestion',{key:((option.value) + "-" + idx),attrs:{"value":option.value,"icon-name":option.icon}},[_vm._t("option",[_vm._v("\n              "+_vm._s(option[_vm.optionTextField])+"\n            ")],null,{ option: option })],2)}):_vm._t("suggestions")],2):_vm._e()],1)]:_vm._t("view",[_vm._v(_vm._s(_vm.inputValue))],null,{ inputValue: _vm.inputValue })],2)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
