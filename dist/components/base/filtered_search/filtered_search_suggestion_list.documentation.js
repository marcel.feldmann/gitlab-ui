var description = "The filtered search suggestion list component is responsible for managing underlying suggestion instances.\nYou obtain the ref for this component and manage suggestion selection via the component public API:\n\n- `getValue()` - Retrieves the current selected suggestion.\n- `nextItem()` - Selects the next suggestion. If last suggestion was selected, selection is cleared.\n- `prevItem()` - Selects the previous suggestion. If first suggestion was selected, selection is cleared.\n\n```html\n<gl-filtered-search-suggestion-list ref=\"suggestions\">\n  <gl-filtered-search-suggestion value=\"foo\">Example suggestion</gl-filtered-search-suggestion>\n  <gl-filtered-search-suggestion value=\"bar\">Example suggestion 2</gl-filtered-search-suggestion>\n</gl-filtered-search-suggestion-list>\n```\n";

var filtered_search_suggestion_list_documentation = {
  description
};

export default filtered_search_suggestion_list_documentation;
