import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  name: 'GlFilteredSearchSuggestionList',
  inject: ['suggestionsListClass'],

  provide() {
    return {
      filteredSearchSuggestionListInstance: this
    };
  },

  props: {
    /**
     * Value to be initially selected in list.
     */
    initialValue: {
      required: false,
      validator: () => true,
      default: null
    }
  },

  data() {
    return {
      activeIdx: -1,
      registeredItems: []
    };
  },

  computed: {
    activeItem() {
      return this.activeIdx > -1 && this.activeIdx < this.registeredItems.length ? this.registeredItems[this.activeIdx] : null;
    },

    listClasses() {
      return [this.suggestionsListClass(), 'dropdown-menu gl-filtered-search-suggestion-list'];
    }

  },
  watch: {
    initialValue(newValue) {
      this.activeIdx = this.registeredItems.findIndex(item => this.valuesMatch(item.value, newValue));
    }

  },
  methods: {
    valuesMatch(firstValue, secondValue) {
      if (!firstValue || !secondValue) return false;
      return typeof firstValue === 'string' ? firstValue.toLowerCase() === secondValue.toLowerCase() : firstValue === secondValue;
    },

    register(item) {
      this.registeredItems.push(item);

      if (this.valuesMatch(item.value, this.initialValue)) {
        this.activeIdx = this.registeredItems.length - 1;
      }
    },

    unregister(item) {
      const idx = this.registeredItems.indexOf(item);

      if (idx !== -1) {
        this.registeredItems.splice(idx, 1);

        if (idx === this.activeIdx) {
          this.activeIdx = -1;
        }
      }
    },

    nextItem() {
      if (this.activeIdx < this.registeredItems.length) {
        this.activeIdx += 1;
      } else {
        this.activeIdx = 0;
      }
    },

    prevItem() {
      if (this.activeIdx >= 0) {
        this.activeIdx -= 1;
      } else {
        this.activeIdx = this.registeredItems.length - 1;
      }
    },

    getValue() {
      return this.activeItem ? this.activeItem.value : null;
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('ul',{class:_vm.listClasses},[_vm._t("default")],2)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
