var description = "The filtered search suggestion component is a wrapper around `GlDropdownItem`, which registers\nsuggestions in a top-level suggestion list:\n\n```html\n<gl-filtered-search-suggestion-list>\n  <gl-filtered-search-suggestion value=\"foo\">Example suggestion</gl-filtered-search-suggestion>\n  <gl-filtered-search-suggestion value=\"bar\">Example suggestion 2</gl-filtered-search-suggestion>\n</gl-filtered-search-suggestion-list>\n```\n";

var filtered_search_suggestion_documentation = {
  description
};

export default filtered_search_suggestion_documentation;
