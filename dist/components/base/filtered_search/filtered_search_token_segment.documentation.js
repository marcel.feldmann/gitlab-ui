var filtered_search_token_segment = "The filtered search token segment is a component for managing token input either via free typing\nor by selecting item through dropdown list\n\n## Usage\n\nThis component is internal and is not intended to be used by `@gitlab/ui` users.\n\n## Internet Explorer 11\n\nThis component uses [`String.prototype.startsWith()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith)\nand [`String.prototype.endsWith()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith)\nunder the hood. Make sure those methods are polyfilled if you plan on using the component on IE11.\n\n> NOTE: These methods are already polyfilled in GitLab: [`app/assets/javascripts/commons/polyfills.js#L15-16`](https://gitlab.com/gitlab-org/gitlab/blob/dc60dee6ed6234dda9f032195577cd8fad9646d8/app/assets/javascripts/commons/polyfills.js#L15-16)\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': filtered_search_token_segment
});

var filtered_search_token_segment_documentation = {
  description
};

export default filtered_search_token_segment_documentation;
