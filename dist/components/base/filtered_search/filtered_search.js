import _cloneDeep from 'lodash/cloneDeep';
import PortalVue from 'portal-vue';
import Vue from 'vue';
import { GlTooltipDirective } from '../../../directives/tooltip';
import GlIcon from '../icon/icon';
import GlSearchBoxByClick from '../search_box_by_click/search_box_by_click';
import GlFilteredSearchTerm from './filtered_search_term';
import { TERM_TOKEN_TYPE, needDenormalization, denormalizeTokens, isEmptyTerm, INTENT_ACTIVATE_PREVIOUS, normalizeTokens } from './filtered_search_utils';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

Vue.use(PortalVue);
let portalUuid = 0;

function createTerm() {
  let data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return {
    type: TERM_TOKEN_TYPE,
    value: {
      data
    }
  };
}

function initialState() {
  return [createTerm()];
}

var script = {
  name: 'GlFilteredSearch',
  components: {
    GlSearchBoxByClick,
    GlIcon
  },
  directives: {
    GlTooltip: GlTooltipDirective
  },

  provide() {
    portalUuid += 1;
    this.portalName = `filters_portal_${portalUuid}`;
    return {
      portalName: this.portalName,
      alignSuggestions: ref => this.alignSuggestions(ref),
      // Return a function reference instead of a prop to work around vue-apollo@3 bug.
      // TODO: This can be reverted once https://github.com/vuejs/vue-apollo/pull/1153
      // has been merged and we consume it, or we upgrade to vue-apollo@4.
      suggestionsListClass: () => this.suggestionsListClass
    };
  },

  inheritAttrs: false,
  props: {
    /**
     * If provided, used as value of filtered search
     */
    value: {
      required: false,
      type: Array,
      default: () => []
    },

    /**
     * Available tokens
     */
    availableTokens: {
      type: Array,
      required: false,
      default: () => []
    },

    /**
     * If provided, used as history items for this component
     */
    placeholder: {
      type: String,
      required: false,
      default: 'Search'
    },
    clearButtonTitle: {
      type: String,
      required: false,
      default: 'Clear'
    },
    historyItems: {
      type: Array,
      required: false,
      default: null
    },

    /**
     * Additional classes to add to the suggestion list menu. NOTE: this not reactive, and the value
     * must be available and fixed when the component is instantiated
     */
    suggestionsListClass: {
      type: [String, Array, Object],
      required: false,
      default: null
    },

    /**
     * Display operators' descriptions instead of their values (e.g., "is" instead of "=").
     */
    showFriendlyText: {
      type: Boolean,
      required: false,
      default: false
    },

    /**
     * HTML attributes to add to the search button
     */
    searchButtonAttributes: {
      type: Object,
      required: false,
      default: () => ({})
    },

    /**
     * HTML attributes to add to the search input
     */
    searchInputAttributes: {
      type: Object,
      required: false,
      default: () => ({})
    }
  },

  data() {
    return {
      tokens: initialState(),
      activeTokenIdx: null,
      suggestionsStyle: {}
    };
  },

  computed: {
    activeToken() {
      return this.tokens[this.activeTokenIdx];
    },

    lastTokenIdx() {
      return this.tokens.length - 1;
    },

    isLastTokenActive() {
      return this.activeTokenIdx === this.lastTokenIdx;
    },

    hasValue() {
      return this.tokens.length > 1 || this.tokens[0].value.data !== '';
    },

    termPlaceholder() {
      return this.hasValue ? null : this.placeholder;
    },

    currentAvailableTokens() {
      return this.availableTokens.filter(token => {
        if (token.disabled) {
          return false;
        }

        if (token.unique) {
          return !this.tokens.find(t => t.type === token.type);
        }

        return true;
      });
    }

  },
  watch: {
    tokens: {
      handler() {
        if (this.tokens.length === 0 || !this.isLastTokenEmpty()) {
          this.tokens.push(createTerm());
        }
        /**
         * Emitted when the tokens (value) changes
         * @property {array} tokens
         */


        this.$emit('input', this.tokens);
      },

      deep: true,
      immediate: true
    }
  },

  mounted() {
    if (this.value.length) {
      this.applyNewValue(_cloneDeep(this.value));
    }
  },

  methods: {
    applyNewValue(newValue) {
      this.tokens = needDenormalization(newValue) ? denormalizeTokens(newValue) : newValue;
    },

    isLastToken(idx) {
      return !this.activeTokenIdx && idx === this.lastTokenIdx;
    },

    isLastTokenEmpty() {
      return isEmptyTerm(this.tokens[this.lastTokenIdx]);
    },

    getTokenEntry(type) {
      return this.availableTokens.find(t => t.type === type);
    },

    getTokenComponent(type) {
      var _this$getTokenEntry;

      return ((_this$getTokenEntry = this.getTokenEntry(type)) === null || _this$getTokenEntry === void 0 ? void 0 : _this$getTokenEntry.token) || GlFilteredSearchTerm;
    },

    activate(idx) {
      this.activeTokenIdx = idx;
    },

    alignSuggestions(ref) {
      const offsetRef = ref.getBoundingClientRect().left;
      const offsetMenu = this.$el.getBoundingClientRect().left;
      const transform = `translateX(${Math.floor(offsetRef - offsetMenu)}px)`;
      this.suggestionsStyle = {
        transform
      };
    },

    deactivate(token) {
      const tokenIdx = this.tokens.indexOf(token);

      if (tokenIdx === -1 || this.activeTokenIdx !== tokenIdx) {
        return;
      }

      if (!this.isLastTokenEmpty()) {
        this.tokens.push(createTerm());
      }

      if (!this.isLastTokenActive && isEmptyTerm(this.activeToken)) {
        this.tokens.splice(tokenIdx, 1);
      }

      this.activeTokenIdx = null;
    },

    destroyToken(idx) {
      let {
        intent
      } = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (this.tokens.length === 1) {
        return;
      }

      this.tokens.splice(idx, 1); // First, attempt to honor the user's activation intent behind the
      // destruction of the token, if any. Otherwise, try to maintain the
      // active state for the token that was active at the time. If that's not
      // possible, make sure no token is active.

      if (intent === INTENT_ACTIVATE_PREVIOUS) {
        // If there is a previous token, activate it; else, activate the first token
        this.activeTokenIdx = Math.max(idx - 1, 0);
      } else if (idx < this.activeTokenIdx) {
        // Preserve the active token's active status (it shifted down one index)
        this.activeTokenIdx -= 1;
      } else if (idx === this.activeTokenIdx) {
        // User destroyed the active token; don't activate another one.
        this.activeTokenIdx = null;
      } // Do nothing if there was no active token, or if idx > this.activeTokenIdx,
      // to preserve the active state of the remaining tokens.

    },

    replaceToken(idx, token) {
      this.$set(this.tokens, idx, { ...token,
        value: {
          data: '',
          ...token.value
        }
      });
      this.activeTokenIdx = idx;
    },

    createTokens(idx) {
      let newStrings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [''];

      if (this.activeTokenIdx !== this.lastTokenIdx && newStrings.length === 1 && newStrings[0] === '') {
        this.activeTokenIdx = this.lastTokenIdx;
        return;
      }

      const newTokens = newStrings.map(data => ({
        type: TERM_TOKEN_TYPE,
        value: {
          data
        }
      }));
      this.tokens.splice(idx + 1, 0, ...newTokens);
      this.activeTokenIdx = idx + newStrings.length;
    },

    completeToken() {
      if (this.activeTokenIdx === this.lastTokenIdx - 1) {
        this.activeTokenIdx = this.lastTokenIdx;
      } else {
        this.activeTokenIdx = null;
      }
    },

    submit() {
      /**
       * Emitted when search is submitted
       * @property {array} tokens
       */
      this.$emit('submit', normalizeTokens(_cloneDeep(this.tokens)));
    },

    clearInput() {
      this.tokens = initialState();
      this.$emit('clearInput');
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('gl-search-box-by-click',_vm._b({attrs:{"value":_vm.tokens,"history-items":_vm.historyItems,"clearable":_vm.hasValue,"search-button-attributes":_vm.searchButtonAttributes,"data-testid":"filtered-search-input"},on:{"submit":_vm.submit,"input":_vm.applyNewValue,"history-item-selected":function($event){return _vm.$emit('history-item-selected', $event)},"clear":function($event){return _vm.$emit('clear')},"clear-history":function($event){return _vm.$emit('clear-history')}},scopedSlots:_vm._u([{key:"history-item",fn:function(slotScope){return [_vm._t("history-item",null,null,slotScope)]}},{key:"input",fn:function(){return [_c('div',{staticClass:"gl-filtered-search-scrollable"},[_vm._l((_vm.tokens),function(token,idx){return [_c(_vm.getTokenComponent(token.type),{key:((token.type) + "-" + idx),ref:"tokens",refInFor:true,tag:"component",staticClass:"gl-filtered-search-item",class:{
            'gl-filtered-search-last-item': _vm.isLastToken(idx),
          },attrs:{"config":_vm.getTokenEntry(token.type),"active":_vm.activeTokenIdx === idx,"available-tokens":_vm.currentAvailableTokens,"current-value":_vm.tokens,"index":idx,"placeholder":_vm.termPlaceholder,"show-friendly-text":_vm.showFriendlyText,"search-input-attributes":_vm.searchInputAttributes,"is-last-token":_vm.isLastToken(idx)},on:{"activate":function($event){return _vm.activate(idx)},"deactivate":function($event){return _vm.deactivate(token)},"destroy":function($event){return _vm.destroyToken(idx, $event)},"replace":function($event){return _vm.replaceToken(idx, $event)},"complete":_vm.completeToken,"submit":_vm.submit,"split":function($event){return _vm.createTokens(idx, $event)}},model:{value:(token.value),callback:function ($$v) {_vm.$set(token, "value", $$v);},expression:"token.value"}})]})],2),_vm._v(" "),_c('portal-target',{key:_vm.activeTokenIdx,ref:"menu",style:(_vm.suggestionsStyle),attrs:{"name":_vm.portalName,"slim":""}})]},proxy:true}],null,true)},'gl-search-box-by-click',_vm.$attrs,false))};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
