var filtered_search_token = "Filtered search token is a helper component, intended to\nsimplify the creation of filters tokens which consist of a title, operators\nand an editable value with autocomplete. This component abstracts token management\nlogic and allows you to focus on implementing autocomplete or view logic.\n\nThis component is not intended to be used outside of the `GlFilteredSearch` component.\n\n## Usage\n\nMake sure to pass `$listeners` to `gl-filtered-search-token`, or route events properly:\n\n```html\n<gl-filtered-search-token\n  title=\"Confidential\"\n  :active=\"active\"\n  :value=\"value\"\n  v-on=\"$listeners\"\n>\n  <template #suggestions>\n    <gl-filtered-search-suggestion value=\"Yes\"><gl-icon name=\"eye-slash\" :size=\"16\"/> Yes</gl-filtered-search-suggestion>\n    <gl-filtered-search-suggestion value=\"No\"><gl-icon name=\"eye\" :size=\"16\"/> No</gl-filtered-search-suggestion>\n  </template>\n</gl-filtered-search-token>\n```\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': filtered_search_token
});

var filtered_search_token_documentation = {
  description
};

export default filtered_search_token_documentation;
