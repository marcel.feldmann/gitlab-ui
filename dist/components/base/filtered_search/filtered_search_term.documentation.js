var filtered_search_term = "The filtered search term is a component for managing \"free input\" in the filtered search component.\nIt is responsible for autocompleting available tokens and \"converting\" to a relevant\ncomponent when an autocomplete item is selected.\n\n## Usage\n\nThis component is internal and is not intended to be used by `@gitlab/ui` users.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': filtered_search_term
});

var filtered_search_term_documentation = {
  description
};

export default filtered_search_term_documentation;
