var description = "`<gl-avatar-link>` decorates `<gl-avatar>` with hyperlink functionality. It accepts the same\nproperties as the `<gl-link>` component and it works in the same way too. The main purpose of this\ncomponent is to apply visual enhancements that makes evident that the user can interact with the\navatar.\n\n## Using the component\n\nWhen wrapping an `<gl-avatar>` component, `<gl-avatar-link>` darkens\nthe border that surrounds the avatar image or fallback text when hovering over it.\n\n```vue\n<gl-avatar-link target=\"blank\" href=\"https://gitlab.com/gitlab-org/gitlab\">\n  <gl-avatar\n    :size=\"32\"\n    :src=\"avatarUrl\"\n  />\n</gl-avatar-link>\n```\n\nWhen wrapping an `<avatar-labeled>` component, `<avatar-link>` underlines\nthe label and sub-label text when hovering over the avatar. It also applies the\nsame effects described in the first example.\n\n```vue\n<gl-avatar-link target=\"blank\" href=\"https://gitlab.com/gitlab-org/gitlab\">\n  <gl-avatar-labeled\n    :size=\"32\"\n    entity-name=\"GitLab\"\n    label=\"GitLab User\"\n    sub-label=\"@gitlab\"\n  />\n</gl-avatar-link>\n```\n";

var avatar_link_documentation = {
  followsDesignSystem: true,
  description
};

export default avatar_link_documentation;
