var description = "Collapse is used to keep pages focused on the overview of what the user can do. Details and\nadditional actions are hidden in the fold, and can be opened if the user wants to interact with\nthose elements.\n";

var collapse_documentation = {
  description
};

export default collapse_documentation;
