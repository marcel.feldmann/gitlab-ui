var nav = "The navigation component is built with flexbox and provides a strong foundation for building all\ntypes of navigation components.\n\n## `GlNavItem`\n\nUse `GlNavItem` to add actionable links (or router links) to your nav. `GlNavItem` wraps [`BNavItem`](https://bootstrap-vue.org/docs/components/navbar#b-nav-item).\n\n## `GlNavItemDropdown`\n\nUse `GlNavItemDropdown` to place dropdown items within your nav.\n`GlNavItemDropdown` wraps [`BNavItemDropdown`](https://bootstrap-vue.org/docs/components/navbar#b-nav-item-dropdown).\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': nav
});

var nav_documentation = {
  description
};

export default nav_documentation;
