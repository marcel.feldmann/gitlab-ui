var skeleton_loader = "Skeleton loaders are to be used when pages or sections can be progressively populated with content,\nsuch as text and images, as they become available. Generally speaking the first batch of content\nwill be the lightest to load and is followed by secondary and tertiary content batches. Each loading\nstep will add in more detail to the page until no skeleton loaders are present anymore. Content\nshould replace skeleton objects immediately when the data is available.\n\nThe skeleton loader component accepts shapes which in return will create a skeleton state with a\nloading animation. Any skeleton state components should be created with\n`<gl-skeleton-loader></gl-skeleton-loader>`. If no shape is passed via the slot the default skeleton\nwill be used. See \"Default\" and \"Default With Custom Props\" examples.\n\n## Progressive Loading\n\nDetermine if progressive loading is available, if it is break apart the skeleton to load data as it\nbecomes readily available. If progessive loading is not available, replace the entire skeleton when\nthe data is available.\n\n## Under the hood\n\nSkeleton Loader is a port of [vue-content-loader](https://github.com/egoist/vue-content-loader).\nSome changes have been made to the code to better suit our codebase, such as removing props and\nrefactoring into a SFC. Please take a look at their documentation and a useful [UI tool](http://danilowoz.com/create-vue-content-loader/)\nfor seeing the code output for `svg` shapes.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': skeleton_loader
});

var skeleton_loader_documentation = {
  followsDesignSystem: true,
  description
};

export default skeleton_loader_documentation;
