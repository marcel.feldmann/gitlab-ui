var toggle = "# Toggle\n\n## Usage\n\nThe toggle component must have a `label` prop to give the toggle button an accessible name.\nTo visually hide the label, provide it with `label-position=\"hidden\"`.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': toggle
});

var toggle_documentation = {
  description,
  followsDesignSystem: true
};

export default toggle_documentation;
