import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const ITEMS_BATCH_SIZE = 10;
const INIT_ITEMS_COUNT = 20;
const colors = ['Violet', 'Indigo', 'Blue', 'Green', 'Yellow', 'Orange', 'Red'];
const planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune'];
let key = 0;

const createItem = () => {
  key += 1;
  return {
    key,
    content: `${colors[key % colors.length]} ${planets[key % planets.length]}`
  };
};

const createArray = num => {
  const res = [];

  for (let i = 0; i < num; i += 1) {
    res.push(createItem());
  }

  return res;
};

var script = {
  data() {
    return {
      isLoading: false,
      fetchedItems: createArray(INIT_ITEMS_COUNT),
      loadTimer: null
    };
  },

  methods: {
    topReached() {
      clearTimeout(this.loadTimer);
      this.isLoading = true;
      this.loadTimer = setTimeout(() => {
        this.fetchedItems.unshift(...createArray(ITEMS_BATCH_SIZE));
        this.isLoading = false;
      }, 1000);
    },

    bottomReached() {
      clearTimeout(this.loadTimer);
      this.isLoading = true;
      this.loadTimer = setTimeout(() => {
        this.fetchedItems.push(...createArray(ITEMS_BATCH_SIZE));
        this.isLoading = false;
      }, 1000);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('gl-infinite-scroll',{attrs:{"max-list-height":285,"fetched-items":_vm.fetchedItems.length},on:{"topReached":_vm.topReached,"bottomReached":_vm.bottomReached},scopedSlots:_vm._u([{key:"items",fn:function(){return [_c('ul',{staticClass:"list-group list-group-flushed list-unstyled"},_vm._l((_vm.fetchedItems),function(item){return _c('li',{key:item.key,staticClass:"list-group-item"},[_vm._v("\n        "+_vm._s(item.content)+"\n      ")])}),0)]},proxy:true},{key:"default",fn:function(){return [_c('div',{staticClass:"gl-mt-3"},[(_vm.isLoading)?_c('gl-loading-icon'):_c('span',[_vm._v(_vm._s(_vm.fetchedItems.length)+" items loaded, scroll up or down for more")])],1)]},proxy:true}])})};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
