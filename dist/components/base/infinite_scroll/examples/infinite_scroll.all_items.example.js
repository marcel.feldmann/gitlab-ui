import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const FETCHED_ITEMS = 5;
var script = {
  data() {
    return {
      fetchedItems: FETCHED_ITEMS
    };
  }

};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('gl-infinite-scroll',{attrs:{"fetched-items":_vm.fetchedItems},scopedSlots:_vm._u([{key:"items",fn:function(){return [_c('ul',{staticClass:"list-group list-group-flushed list-unstyled"},_vm._l((_vm.fetchedItems),function(item){return _c('li',{key:item,staticClass:"list-group-item"},[_vm._v("Item #"+_vm._s(item))])}),0)]},proxy:true},{key:"default",fn:function(){return [_c('div',{staticClass:"gl-mt-3"},[_c('span',[_vm._v("All items loaded")])])]},proxy:true}])})};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
