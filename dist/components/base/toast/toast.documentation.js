var description = "\nToasts are used to display system messages. The messages are short and straightforward. It may\ncontain a dismiss button, and an action button depending on the situation.\n\n## Using the plugin\n\nIn order to use the plugin, it needs to be included in your application with `Vue.use`.\n\n```js\n// myApp.js\n\nimport { GlToast } from '@gitlab/ui';\n\n// Note, this has to be done before `Vue.new()`\nVue.use(GlToast);\n```\n\nOnce included in your application, the toast plugin is globally available.\n\n```js\n// myComponent.vue\n\nthis.$toast.show('Hello GitLab!');\n```\n\nBelow is an example with options\n\n```js\n// myComponent.vue\n\nthis.$toast.show('This is a toast with an option.', {\n  action: {\n    text: 'Undo',\n    onClick: () => { ... },\n  },\n});\n```\n\n### Options\n\nBelow are the options you can pass to create a toast\n\n| **Option**    | **Type**      | **Default** | **Description**                          |\n| ------------- | ------------- | ----------- | ---------------------------------------- |\n| autoHideDelay | Number        | 5000        | Display time of the toast in millisecond |\n| action        | Object        | close       | Add single actions to toast              |\n| toastClass    | String, Array | 'gl-toast'  | Custom css class name of the toast       |\n| onComplete    | Function      | null        | Trigger when toast is completed          |\n";

var toast_documentation = {
  followsDesignSystem: true,
  description
};

export default toast_documentation;
