var description = "**Note:** This needs a `v-model` property to work correctly.\nSee [this issue](https://github.com/bootstrap-vue/bootstrap-vue/issues/1915) on Bootstrap Vue for\nmore information.\n";

var form_textarea_documentation = {
  description
};

export default form_textarea_documentation;
