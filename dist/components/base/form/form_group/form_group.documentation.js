var description = "Form group adds structure to forms.\n";

var form_group_documentation = {
  description
};

export default form_group_documentation;
