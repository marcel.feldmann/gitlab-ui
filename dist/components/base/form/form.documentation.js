var description = "A wrapper element for `gl-form-*` input elements. Supports `@submit` and `@reset` events, as well as\ncontrolling validation settings for form inputs.\n";

var form_documentation = {
  description
};

export default form_documentation;
