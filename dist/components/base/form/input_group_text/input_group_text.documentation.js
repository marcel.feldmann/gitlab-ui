var input_group_text = "### Under the hood\n\nUses [`BInputGroupText`](https://bootstrap-vue.org/docs/components/input-group#comp-ref-b-input-group-text)\nfrom Vue Boostrap\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': input_group_text
});

var input_group_text_documentation = {
  description
};

export default input_group_text_documentation;
