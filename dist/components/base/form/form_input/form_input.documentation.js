var description = "General user input to be used in forms.\n";

var form_input_documentation = {
  description,
  followsDesignSystem: true
};

export default form_input_documentation;
