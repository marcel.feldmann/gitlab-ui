import _uniqueId from 'lodash/uniqueId';
import GlDropdownItem from '../../dropdown/dropdown_item';
import GlFormGroup from '../form_group/form_group';
import GlFormInput from '../form_input/form_input';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  name: 'GlFormCombobox',
  components: {
    GlDropdownItem,
    GlFormGroup,
    GlFormInput
  },
  model: {
    prop: 'value',
    event: 'input'
  },
  props: {
    labelText: {
      type: String,
      required: true
    },
    labelSrOnly: {
      type: Boolean,
      required: false,
      default: false
    },
    tokenList: {
      type: Array,
      required: true
    },
    value: {
      type: String,
      required: true
    }
  },

  data() {
    return {
      results: [],
      arrowCounter: -1,
      userDismissedResults: false,
      suggestionsId: _uniqueId('token-suggestions-'),
      inputId: _uniqueId('token-input-')
    };
  },

  computed: {
    ariaExpanded() {
      return this.showSuggestions.toString();
    },

    showAutocomplete() {
      return this.showSuggestions ? 'off' : 'on';
    },

    showSuggestions() {
      return this.results.length > 0;
    }

  },

  mounted() {
    document.addEventListener('click', this.handleClickOutside);
  },

  destroyed() {
    document.removeEventListener('click', this.handleClickOutside);
  },

  methods: {
    closeSuggestions() {
      this.results = [];
      this.arrowCounter = -1;
    },

    handleClickOutside(event) {
      if (!this.$el.contains(event.target)) {
        this.closeSuggestions();
      }
    },

    onArrowDown() {
      const newCount = this.arrowCounter + 1;

      if (newCount >= this.results.length) {
        this.arrowCounter = 0;
        return;
      }

      this.arrowCounter = newCount;
    },

    onArrowUp() {
      const newCount = this.arrowCounter - 1;

      if (newCount < 0) {
        this.arrowCounter = this.results.length - 1;
        return;
      }

      this.arrowCounter = newCount;
    },

    onEnter() {
      const currentToken = this.results[this.arrowCounter] || this.value;
      this.selectToken(currentToken);
    },

    onEsc() {
      if (!this.showSuggestions) {
        this.$emit('input', '');
      }

      this.closeSuggestions();
      this.userDismissedResults = true;
    },

    onEntry(value) {
      this.$emit('input', value);
      this.userDismissedResults = false; // short circuit so that we don't false match on empty string

      if (value.length < 1) {
        this.closeSuggestions();
        return;
      }

      const filteredTokens = this.tokenList.filter(token => token.toLowerCase().includes(value.toLowerCase()));

      if (filteredTokens.length) {
        this.openSuggestions(filteredTokens);
      } else {
        this.closeSuggestions();
      }
    },

    openSuggestions(filteredResults) {
      this.results = filteredResults;
    },

    selectToken(value) {
      this.$emit('input', value);
      this.closeSuggestions();
      /**
       * Emitted when a value is selected.
       * @event value-selected
       */

      this.$emit('value-selected', value);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"gl-form-combobox dropdown",attrs:{"role":"combobox","aria-owns":_vm.suggestionsId,"aria-expanded":_vm.ariaExpanded}},[_c('gl-form-group',{attrs:{"label":_vm.labelText,"label-for":_vm.inputId,"label-sr-only":_vm.labelSrOnly}},[_c('gl-form-input',{attrs:{"id":_vm.inputId,"value":_vm.value,"type":"text","role":"searchbox","autocomplete":_vm.showAutocomplete,"aria-autocomplete":"list","aria-controls":_vm.suggestionsId,"aria-haspopup":"listbox"},on:{"input":_vm.onEntry,"keydown":[function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"down",40,$event.key,["Down","ArrowDown"])){ return null; }return _vm.onArrowDown($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"up",38,$event.key,["Up","ArrowUp"])){ return null; }return _vm.onArrowUp($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }$event.preventDefault();return _vm.onEnter($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"esc",27,$event.key,["Esc","Escape"])){ return null; }$event.stopPropagation();return _vm.onEsc($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"tab",9,$event.key,"Tab")){ return null; }return _vm.closeSuggestions($event)}]}})],1),_vm._v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.showSuggestions && !_vm.userDismissedResults),expression:"showSuggestions && !userDismissedResults"}],staticClass:"dropdown-menu dropdown-full-width",class:{ 'show-dropdown': _vm.showSuggestions },attrs:{"id":_vm.suggestionsId,"data-testid":"combobox-dropdown"}},_vm._l((_vm.results),function(result,i){return _c('gl-dropdown-item',{key:i,class:{ 'highlight-dropdown': i === _vm.arrowCounter },attrs:{"role":"option","aria-selected":i === _vm.arrowCounter,"tabindex":"-1"},on:{"click":function($event){return _vm.selectToken(result)}}},[_vm._v("\n      "+_vm._s(result)+"\n    ")])}),1)],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
