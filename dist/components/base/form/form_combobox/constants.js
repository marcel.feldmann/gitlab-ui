const tokenList = ['giraffe', 'dog', 'dodo', 'komodo dragon', 'hippo', 'platypus', 'jackalope', 'quetzal', 'badger', 'vicuña', 'whale', 'xenarthra'];
const labelText = 'Animals We Tolerate';

export { labelText, tokenList };
