var description = "Form checkbox groups for general use inside forms.\n\n## Stacked\n\nBy default, the GitLab Design guide mandates the `<gl-form-checkbox-group>` be `stacked` and is\nnon-changeable at this time.\n";

var form_checkbox_documentation = {
  description,
  followsDesignSystem: true
};

export default form_checkbox_documentation;
