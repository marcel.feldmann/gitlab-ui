const CHECKED_STATE = {
  UNCHECKED: 0,
  INDETERMINATE: 1,
  CHECKED: 2
};
const V_MODEL = {
  PROP: 'value',
  EVENT: 'change'
};
const QA_PREFIX = 'form_checkbox_tree_node_';

export { CHECKED_STATE, QA_PREFIX, V_MODEL };
