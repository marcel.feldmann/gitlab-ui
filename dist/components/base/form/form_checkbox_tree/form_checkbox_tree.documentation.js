var form_checkbox_tree = "`GlFormCheckboxTree` lets you display an options structure where any option can have n-level of\nchildren. It can be useful for creating a search filter that has nested facets.\n\n## Usage\n\n`GlFormCheckboxTree` accepts an `options` prop representing the available options in the form of\nan n-level deep tree. Each option should have a `value` and can have optional\n`label` and `children`. If `label` is omitted, `value` is used as the checkbox's label.\nHere's a simple `options` tree for example:\n\n```js\n[\n  {\n    label: 'Option #1',\n    value: 1,\n    children: [\n      {\n        label: 'Option #2',\n        value: 2,\n      },\n    ],\n  },\n  {\n    label: 'Option #3',\n    value: 3,\n  },\n]\n```\n\n`GlFormCheckboxTree` exposes the selected options via a `v-model` which is being kept in sync with\nthe `change` event.\n\n## Dos and don'ts\n\n### Don't\n\nWhen rendering a `GlFormCheckboxTree` with pre-selected options, all the selected values should be\npassed to the component via the `v-model`/`value` property. For example, with the options tree\nabove, if you wanted options `1` and `2` to be pre-selected, make sure that they are both included\nin the initial value, don't rely on the component to infer initially checked boxes by only passing\n`1` or `2`.\n\n```html\n<!-- Good -->\n<gl-form-checkbox-tree\n  :value=\"[1, 2]\"\n  :options=\"[\n    {\n      value: 1,\n      children: [\n        {\n          value: 2,\n        },\n      ],\n    },\n  ]\"\n/>\n\n<!-- Bad -->\n<gl-form-checkbox-tree\n  :value=\"[1]\"\n  :options=\"[\n    {\n      value: 1,\n      children: [\n        {\n          value: 2,\n        },\n      ],\n    },\n  ]\"\n/>\n```\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': form_checkbox_tree
});

var form_checkbox_tree_documentation = {
  description
};

export default form_checkbox_tree_documentation;
