var description = "`GlFormRadio` components can be used directly, or via a `GlFormRadioGroup`.\n\nBelow is an example which demonstrates the direct approach. For examples using\n`GlFormRadioGroup`, see the documentation for that component.\n\n```html\n<script>\n  export default {\n    data() {\n      return {\n        selected: 'yes',\n      };\n    },\n  };\n</script>\n\n<template>\n  <div>\n    <gl-form-radio v-model=\"selected\" value=\"yes\">Yes</gl-form-radio>\n    <gl-form-radio v-model=\"selected\" value=\"no\">No</gl-form-radio>\n  </div>\n</template>\n```\n";

var form_radio_documentation = {
  description,
  followsDesignSystem: true
};

export default form_radio_documentation;
