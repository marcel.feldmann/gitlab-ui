var description = "Form select component used to select from group of options in a form.\n";

var form_select_documentation = {
  description,
  followDesignSystem: true
};

export default form_select_documentation;
