var form_text = "# Usage\n\nThe component used to render form group descriptions. Useful for when you need\nto add additional form text elements to a form group.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': form_text
});

var form_text_documentation = {
  description,
  bootstrapComponent: 'b-form-text',
  propsInfo: {}
};

export default form_text_documentation;
