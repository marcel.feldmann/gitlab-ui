var keyset_pagination = "Pagination is used to help users parse a large number of items on a page,\nwhenever there are too many results to show at once. Pagination breaks up\nresults into several pages with controls for navigating forward and backward, or\nto a specific page.\n\n## Usage\n\nThe simplest way to use `GlKeysetPagination` with a paginated GraphQL response\nis to `v-bind` to the\n[`PageInfo`](https://docs.gitlab.com/ee/api/graphql/reference/#pageinfo) type\nreturned by the endpoint:\n\n```html\n<gl-keyset-pagination v-bind=\"pageInfo\" />\n```\n\nThis is possible because the default field names of the `PageInfo` type align\nwith the `props` of this component.\n\n## Dos and don'ts\n\n**✅ Do** provide the `prevText` and `nextText` props with translatable strings.\nThe default strings (\"Prev\" and \"Next\") are hardcoded in this component and\ncan't be translated.\n\nExample:\n\n```html\n<gl-keyset-pagination v-bind=\"pageInfo\" :prev-text=\"__('Prev')\" :next-text=\"__('Next')\" />\n```\n\n**✅ Do** use this component for paginating GraphQL requests<sup>1</sup> (or any\nendpoint that uses keyset pagination).\n\n**❌ Don't** use this component for paginating REST requests<sup>1</sup> (or any\nendpoint that uses offset pagination).\n\nFor offset pagination, use the [`GlPagination`\ncomponent](/?path=/story/base-pagination--default) instead.\n\nFor more information on the difference between offset and keyset pagination see\n[our documentation on GraphQL\npagination](https://docs.gitlab.com/ee/development/graphql_guide/pagination.html).\n\n<small><sup>1</sup>There's no intrinsic reason why GraphQL endpoints can't\nsupport offset pagination (in fact, [the official\ndocumentation](https://graphql.org/learn/pagination/#pagination-and-edges) shows\nan example offset pagination implementation) or why REST endpoints can't support\nkeyset pagination. This is simply how we've chosen to implement our REST and\nGraphQL endpoints at GitLab.</small>\n\n## Pajamas reference\n\n<https://design.gitlab.com/components/pagination>\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': keyset_pagination
});

var keyset_pagination_documentation = {
  description,
  followsDesignSystem: true
};

export default keyset_pagination_documentation;
