import description from './label';

var label_documentation = {
  followsDesignSystem: true,
  description
};

export default label_documentation;
