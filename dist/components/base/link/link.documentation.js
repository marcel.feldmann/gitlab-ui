var description = "## Security\n\nThis component implements a few security measures to make it as safe as possible by default.\nSee [SafeLinkDirective docs] for more details.\n\n### Linking to an unsafe URL\n\nIf you're trying to link to a location considered unsafe by the `SafeLink` directive (when rendering\na download link with a [Data URL] for example), you'll need to bypass the `href` attribute's\nsanitization. This component exposes the `is-unsafe-link` prop for that purpose.\n\n> **Warning:** Only disable URL sanitization when absolutely necessary.\n\n```html\n<gl-link\n  is-unsafe-link\n  download=\"file.txt\"\n  href=\"data:text/plain;charset=utf-8,GitLab%20is%20awesome\">Download</gl-link>\n```\n\n[SafeLinkDirective docs]: https://gitlab-org.gitlab.io/gitlab-ui/?path=/docs/directives-safe-link-directive--default\n[Data URL]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs\n";

var link_documentation = {
  description
};

export default link_documentation;
