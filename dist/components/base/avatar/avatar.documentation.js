var description = "Avatars are used to represent a unique entity, be it a person, a group, or a project.\n";

var avatar_documentation = {
  followsDesignSystem: true,
  description
};

export default avatar_documentation;
