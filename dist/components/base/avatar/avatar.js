import { avatarSizeOptions, avatarShapeOptions } from '../../../utils/constants';
import { getAvatarChar } from '../../../utils/string_utils';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const IDENTICON_BG_COUNT = 7;
var script = {
  props: {
    entityId: {
      type: Number,
      required: false,
      default: 0
    },
    entityName: {
      type: String,
      required: false,
      default: ''
    },
    src: {
      type: String,
      required: false,
      default: ''
    },
    alt: {
      type: String,
      required: false,
      default: 'avatar'
    },
    size: {
      type: Number,
      required: false,
      default: avatarSizeOptions[1],
      validator: value => {
        const isValidSize = avatarSizeOptions.includes(value);

        if (!isValidSize) {
          /* eslint-disable-next-line no-console */
          console.error(`Avatar size should be one of [${avatarSizeOptions}], received: ${value}`);
        }

        return isValidSize;
      }
    },
    shape: {
      type: String,
      required: false,
      default: avatarShapeOptions.circle
    }
  },
  computed: {
    sizeClass() {
      return `gl-avatar-s${this.size}`;
    },

    isCircle() {
      return this.shape === avatarShapeOptions.circle;
    },

    identiconBackgroundClass() {
      /*
       * Gets a number between 1-7 depending on the 'entityId'.
       * Gets the remainder after dividing the 'entityId' by the number of available backgrounds.
       */
      const type = this.entityId % IDENTICON_BG_COUNT + 1;
      return `gl-avatar-identicon-bg${type}`;
    },

    identiconText() {
      return getAvatarChar(this.entityName);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.src)?_c('img',{class:['gl-avatar', { 'gl-avatar-circle': _vm.isCircle }, _vm.sizeClass],attrs:{"src":_vm.src,"alt":_vm.alt}}):_c('div',{class:[
    'gl-avatar gl-avatar-identicon',
    { 'gl-avatar-circle': _vm.isCircle },
    _vm.sizeClass,
    _vm.identiconBackgroundClass ]},[_vm._v("\n  "+_vm._s(_vm.identiconText)+"\n")])};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
