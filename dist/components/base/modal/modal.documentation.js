var description = "Modals are used to reveal critical information, show information without losing context, or when the\nsystem requires a user response. Modals can also fragment a complex workflow into simpler steps and\nshould serve a single purpose dedicated to completing the user’s task.\n\n## VModel\n\nYou can use the `v-model` directive to control the modal’s visibility. The `v-model`\ndirective interfaces with the `visible` property and the `@change` event.\n\n## Deprecation Warning\n\nWe are deprecating the `modal-ok` and `modal-cancel` slots. We are also changing the way the\n`modal-footer` slot content is populated. This is in order to align this component with the design\nsystem.\n\nThe `modal-footer` slot should only be populated via props: `action-primary`, `action-secondary` and\n`action-cancel`. These props allow you to handle how a primary, secondary and cancel button will\nbehave in the modals footer. The props receive an object as such:\n\n```js\n{\n  text: 'Save Changes',\n  attributes: {\n    variant: 'confirm',\n    disabled: this.someState,\n    class: 'some-class',\n    ...\n  }\n}\n```\n";

var modal_documentation = {
  followsDesignSystem: true,
  description
};

export default modal_documentation;
