var markdown = "The `GlMarkdown` component styles markdown-generated HTML following the Pajamas Documentation Markdown\n[styling specifications](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit---Beta?node-id=542%3A2).\n\n## Usage\n\nYou can use the `GlMarkdown` component in two ways.\n\n### Vue component\n\n```html\n<script>\nimport { GlMarkdown } from '@gitlab/ui';\n\nexport default {\n  components: {\n    GlMarkdown,\n  }\n}\n</script>\n<template>\n  <gl-markdown>\n    <!-- All the content inside gl-markdown will inherit the documentation markdown styles -->\n  </gl-markdown>\n</template>\n```\n\n### `gl-markdown` class selector\n\nFollow the [GitLab UI CSS guidelines](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/doc/css.md)\nto include GitLab UI CSS in your application. Afterwards, you can apply the `gl-markdown` class\nselector to the root element that contains the markdown-generated HTML.\n\n```html\n<body class=\"gl-markdown\">\n  <!-- All the content inside body.gl-markdown will inherit the documentation markdown styles -->\n</body>\n```\n\n### Compact markdown\n\nSet the `compact` property to true in `GlMarkdown` to apply the compact markdown styles.\n\n```html\n<gl-markdown compact></gl-compact>\n```\n\nYou can also append the `gl-compact-markdown` class selector after `gl-markdown` in markdown-generated\nHTML.\n\n```html\n<body class=\"gl-markdown gl-compact-markdown\">\n</body>\n```\n\n<!--\n## Browser compatibility\n\nIf the component requires any polyfill or fallback on certain browsers, describe those requirements\nhere.\n-->\n\n<!--\n## Edge cases\n\nIf the component has some known limitations, describe them here.\n-->\n\n<!--\n## Deprecation warning\n\nIf and when this component introduced API changes that would require deprecating old APIs, describe\nthe changes here, and provide a migration paths to the new API.\n-->\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': markdown
});

var markdown_documentation = {
  description
};

export default markdown_documentation;
