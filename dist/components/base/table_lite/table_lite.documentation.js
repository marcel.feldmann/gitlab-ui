var table_lite = "## Usage\n\nThe `GlTableLite` component wraps BootstrapVue `BTableLite` component.\n`BTableLite` provides a variety of slots for custom data rendering. You can learn\nmore about them in the\n[component documentation](https://bootstrap-vue.org/docs/components/table#light-weight-tables).\n\n## `GlTable` vs. `GlTableLite`\n\n`GlTableLite` adds less payload to the pagebundle than `GlTable`.\nWhen possible `GlTableLite` should be preferred over `GlTable`.\n\nThe `GlTableLite` component provides all of the styling and formatting features of\n`GlTable` (including row details and stacked support), while excluding the following features:\n\n- Filtering\n- Sorting\n- Pagination\n- Items provider support\n- Selectable rows\n- Busy table state and styling\n- Fixed top and bottom rows\n- Empty row support\n\n## Internationalization\n\nTo ensure we internationalize field labels, always pass an array of objects for the `fields` prop,\nlike mentioned in the implementation example.\n\n_Full documentation for the `field` prop [here.](https://bootstrap-vue.org/docs/components/table#fields-column-definitions)_\n\n## Implementation example\n\n```html\n<script>\nexport default {\n  fields: [\n    {\n      key: 'column_one',\n      label: __('Column One'),\n      thClass: 'w-60p',\n      tdClass: 'table-col d-flex'\n    },\n    {\n      key: 'col_2',\n      label: __('Column 2'),\n      thClass: 'w-15p',\n      tdClass: 'table-col d-flex'\n    },\n  ];\n}\n</script>\n<template>\n  <gl-table-lite\n    :items=\"items\"\n    :fields=\"$options.fields\"\n  >\n    <template #head(column_one)>\n      <div>Column One</div><!-- This is the column head for the first object in `fields` -->\n    </template>\n\n    <template #cell(column_one)>\n      This is the template for column data belonging to the first object\n    </template>\n\n  </gl-table-lite>\n</template>\n```\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': table_lite
});

var table_lite_documentation = {
  description,
  bootstrapComponent: 'b-table-lite'
};

export default table_lite_documentation;
