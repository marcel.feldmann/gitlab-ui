var description = "## Usage\n\nThis component provides two `<slot>` elements for customization. The first is a `<slot #avatar>`\nso an avatar can appear before the first breadcrumb. The second is a `<slot #separator>` to\ncustomize the icon that appears between each breadcrumb.\n\n**note:** the component supports passing the property `to` in the list items to enable navigation\nthrough `vue-router`\n\n### Example\n\nYou can use any kind of separator you want in the slot, like below, which uses `<svg>`\nto draw a `/`\n\n```html\n<gl-breadcrumb :items=\"items\">\n  <template #avatar>\n    <img \n      alt=\"\"\n      class=\"gl-breadcrumb-avatar-tile\"\n      src=\"https://assets.gitlab-static.net/uploads/-/system/group/avatar/9970/logo-extra-whitespace.png?width=16\"\n      width=\"16\"\n      height=\"16\"\n    />\n  </template>\n  <template #separator>\n    <svg height=\"16\">\n      <line x1=\"100%\" y1=\"0\" x2=\"0\" y2=\"100%\" stroke=\"gray\" />\n    </svg>\n  </template>\n</gl-breadcrumb>\n```\n";

var breadcrumb_documentation = {
  followsDesignSystem: true,
  description
};

export default breadcrumb_documentation;
