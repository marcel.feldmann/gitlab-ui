import { BBreadcrumb } from 'bootstrap-vue/esm/index.js';
import GlIcon from '../icon/icon';
import GlButton from '../button/button';
import { GlTooltipDirective } from '../../../directives/tooltip';
import GlBreadcrumbItem from './breadcrumb_item';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

const COLLAPSE_AT_SIZE = 4;
var script = {
  components: {
    BBreadcrumb,
    GlIcon,
    GlButton,
    GlBreadcrumbItem
  },
  directives: {
    GlTooltip: GlTooltipDirective
  },
  inheritAttrs: false,
  props: {
    /**
     * The breadcrumb items to be displayed as links.
     */
    items: {
      type: Array,
      required: true,
      default: () => [{
        text: '',
        href: ''
      }],
      validator: links => {
        return links.every(link => {
          const keys = Object.keys(link);
          return keys.includes('text') && (keys.includes('href') || keys.includes('to'));
        });
      }
    }
  },

  data() {
    return {
      isListCollapsed: true
    };
  },

  computed: {
    breadcrumbsSize() {
      return this.items.length;
    },

    hasCollapsible() {
      return this.breadcrumbsSize > COLLAPSE_AT_SIZE;
    },

    nonCollapsibleIndices() {
      return [0, this.breadcrumbsSize - 1, this.breadcrumbsSize - 2];
    }

  },
  methods: {
    isFirstItem(index) {
      return index === 0;
    },

    isLastItem(index) {
      return index === this.breadcrumbsSize - 1;
    },

    expandBreadcrumbs() {
      this.isListCollapsed = false;

      try {
        this.$refs.firstItem[0].querySelector('a').focus();
      } catch (e) {
        /* eslint-disable-next-line no-console */
        console.error(`Failed to set focus on the first breadcrumb item.`);
      }
    },

    showCollapsedBreadcrumbsExpander(index) {
      return index === 0 && this.hasCollapsible && this.isListCollapsed;
    },

    isItemCollapsed(index) {
      return this.hasCollapsible && this.isListCollapsed && !this.nonCollapsibleIndices.includes(index);
    },

    getAriaCurrentAttr(index) {
      return this.isLastItem(index) ? 'page' : false;
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"gl-breadcrumbs",attrs:{"aria-label":"Breadcrumb"}},[_vm._t("avatar"),_vm._v(" "),_c('b-breadcrumb',_vm._g(_vm._b({staticClass:"gl-breadcrumb-list"},'b-breadcrumb',_vm.$attrs,false),_vm.$listeners),[_vm._l((_vm.items),function(item,index){return [_c('gl-breadcrumb-item',{key:index,ref:_vm.isFirstItem(index) ? 'firstItem' : null,refInFor:true,class:{ 'gl-display-none': _vm.isItemCollapsed(index) },attrs:{"text":item.text,"href":item.href,"to":item.to,"aria-current":_vm.getAriaCurrentAttr(index)}},[_c('span',[_vm._v(_vm._s(item.text))]),_vm._v(" "),(!_vm.isLastItem(index))?_c('span',{key:(index + " " + (item.text)),staticClass:"gl-breadcrumb-separator",attrs:{"data-testid":"separator"}},[_vm._t("separator",[_c('gl-icon',{attrs:{"name":"chevron-right"}})])],2):_vm._e()]),_vm._v(" "),(_vm.showCollapsedBreadcrumbsExpander(index))?[_c('gl-button',{directives:[{name:"gl-tooltip",rawName:"v-gl-tooltip.hover",value:('Show all breadcrumbs'),expression:"'Show all breadcrumbs'",modifiers:{"hover":true}}],attrs:{"aria-label":"Show all breadcrumbs","data-testid":"collapsed-expander","icon":"ellipsis_h","category":"primary"},on:{"click":_vm.expandBreadcrumbs}}),_vm._v(" "),_c('span',{key:"expander",staticClass:"gl-display-inline-flex gl-text-gray-500",attrs:{"data-testid":"expander-separator"}},[_vm._t("separator",[_c('gl-icon',{attrs:{"name":"chevron-right"}})])],2)]:_vm._e()]})],2)],2)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
export { COLLAPSE_AT_SIZE };
