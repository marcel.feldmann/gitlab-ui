var description = "Button group of equal options where only one can be selected and active.\n";

var segmented_control_documentation = {
  followsDesignSystem: false,
  description
};

export default segmented_control_documentation;
