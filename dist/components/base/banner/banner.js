import { bannerVariants } from '../../../utils/constants';
import GlButton from '../button/button';
import GlCard from '../card/card';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  name: 'GlBanner',
  components: {
    GlButton,
    GlCard
  },
  props: {
    /**
     * Used to set the title of the banner.
     */
    title: {
      type: String,
      required: true
    },

    /**
     * Text for the submit button.
     */
    buttonText: {
      type: String,
      required: true
    },

    /**
     * Link for the submit button.
     */
    buttonLink: {
      type: String,
      required: false,
      default: null
    },

    /**
     * The illustration's URL.
     */
    svgPath: {
      type: String,
      required: false,
      default: null
    },

    /**
     * The variant of the banner.
     */
    variant: {
      type: String,
      required: false,
      default: bannerVariants[0],

      validator(value) {
        return bannerVariants.includes(value);
      }

    },

    /**
     * Removes the border for banners embedded in content.
     */
    embedded: {
      type: Boolean,
      required: false,
      default: null
    }
  },
  computed: {
    isIntroducing() {
      return this.variant === bannerVariants[1];
    }

  },
  methods: {
    handleClose() {
      /**
       * Emitted when the close button is clicked.
       *
       * @event close
       * @type {object}
       */
      this.$emit('close');
    },

    primaryButtonClicked() {
      /**
       * Emitted when the primary action button is clicked.
       *
       * @event primary
       * @type {object}
       */
      this.$emit('primary');
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('gl-card',{staticClass:"gl-px-8 gl-py-6 gl-line-height-20",class:{ 'gl-banner-introduction': _vm.isIntroducing, 'gl-border-none!': _vm.embedded },attrs:{"body-class":"gl-display-flex gl-p-0!"}},[(_vm.svgPath)?_c('div',{staticClass:"gl-banner-illustration"},[_c('img',{attrs:{"src":_vm.svgPath,"alt":"","role":"presentation"}})]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"gl-banner-content"},[_c('h1',{staticClass:"gl-banner-title"},[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),_vm._t("default"),_vm._v(" "),_c('gl-button',{attrs:{"variant":"confirm","category":"primary","data-testid":"gl-banner-primary-button","href":_vm.buttonLink},on:{"click":_vm.primaryButtonClicked}},[_vm._v(_vm._s(_vm.buttonText))]),_vm._v(" "),_vm._t("actions")],2),_vm._v(" "),_c('gl-button',{staticClass:"gl-banner-close",attrs:{"variant":_vm.isIntroducing ? 'confirm' : 'default',"category":"tertiary","size":"small","icon":"close","aria-label":"Close banner"},on:{"click":_vm.handleClose}})],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
