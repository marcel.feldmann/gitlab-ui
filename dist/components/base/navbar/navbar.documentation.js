var navbar = "### Navbar\n\nThe component <navbar> is a wrapper that positions branding, navigation, and other elements into a\nconcise header.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': navbar
});

var navbar_documentation = {
  description
};

export default navbar_documentation;
