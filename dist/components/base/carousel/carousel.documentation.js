var description = "The carousel is a slideshow for cycling through a series of content, built with CSS 3D\ntransforms. It works with a series of images, text, or custom markup. It also includes support\nfor previous/next controls and indicators.\n";

var carousel_documentation = {
  description,
  bootstrapComponent: 'b-carousel'
};

export default carousel_documentation;
