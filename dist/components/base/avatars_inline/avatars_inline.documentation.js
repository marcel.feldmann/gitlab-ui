var avatars_inline = "Use `<avatars-inline />` to display multiple avatars organized in a single row.\n\n### Basic usage\n\nThe `avatars` property accepts an array of objects that contains the avatar properties. By default,\n`<avatars-inline />` expects each object contained in the array to have the same shape as the\nproperties of the `<avatar />` component. You can customize the display of each avatar by\noverriding the default slot:\n\n```html\n<gl-avatars-inline :avatars=\"avatars\">\n  <template #avatar=\"{ avatar }\">\n    <gl-avatar-link v-gl-tooltip target=\"blank\" :href=\"avatar.href\" :title=\"avatar.tooltip\">\n      <gl-avatar :src=\"avatar.src\" :size=\"32\" />\n    </gl-avatar-link>\n  </template>\n</gl-avatars-inline>\n```\n\nIn the example above, the avatars displayed inside `<avatars-inline />` are links pointing to a URL\nstored in each avatar object. Each avatar also displays a tooltip. If you override\n`<inline-avatars />` default display, you can pass an array of objects with any desired shape to\nthe `avatars` property.\n\n### Collapsing\n\nWhen the `collapse` property value is `true` and the `maxVisible` property value is a number less\nthan the length of the `avatars` property array, `<avatars-inline>` will hide the overflown avatars\nand display a badge instead.\n\n### Badge description in screen readers\n\nThe `badgeSrOnlyText` property provides a meaningful description of the badge that appears\nwhen avatars are collapsed for screen reader users.\n\n### Supported sizes\n\n`<avatars-inline>` only supports avatars with `24` or `32` size.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': avatars_inline
});

var avatars_inline_documentation = {
  followsDesignSystem: true,
  description
};

export default avatars_inline_documentation;
