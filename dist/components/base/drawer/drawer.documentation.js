var description = "The drawer is used to show more information about a certain resource in the UI and potentially\nhandle actions on the information.\n\n### By default\n\n```html\n<gl-drawer :open=\"open\" @close=\"close\">\n  <template #header>Your Title</template>\n  <template>\n   ...children\n  </template>\n</gl-drawer>\n```\n\n- `v-bind:open` will be a boolean you will pass to `gl-drawer` and `@close` is a listener that will\nbe a function that will toggle open to `false`.\n";

var drawer_documentation = {
  followsDesignSystem: true,
  description
};

export default drawer_documentation;
