var card = "Cards are a flexible component used to display content and actions in a variety of contexts.\nThey are generally restricted to a single topic and it should be easy for users to scan relevant and\nactionable information. Content, such as images and text, should be positioned within them in a\nmanner that demonstrates their intended hierarchy.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': card
});

var card_documentation = {
  followsDesignSystem: true,
  description
};

export default card_documentation;
