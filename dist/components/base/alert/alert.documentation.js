var alert = "Alerts allow the application to pass along relevant system information to the\nuser without impeding their journey. Alerts are system generated and may or may\nnot be derived by a user’s action.\n\n## Dismissible alerts\n\nAlerts don't handle their own visibility, so it's the parent component's\nresponsbility to listen for the `dismiss` event and hide the alert in some way.\nFor example:\n\n```html\n<script>\n  ...\n  computed: {\n    shouldShowAlert() {\n      return !this.isAlertDismissed && this.someOtherCondition();\n    },\n  },\n  ...\n</script>\n\n<template>\n  ...\n  <gl-alert v-if=\"shouldShowAlert\" @dismiss=\"isAlertDismissed = true\">\n    An important message\n  </gl-alert>\n  ...\n</template>\n```\n\n## Sticky alerts\n\nAny alert can use `position: sticky`, however it should be limited to critical alerts where keeping\nthe alert visually in context is necessary or when alerts are injected into a page and might\notherwise go unnoticed.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': alert
});

var alert_documentation = {
  description,
  followsDesignSystem: true
};

export default alert_documentation;
