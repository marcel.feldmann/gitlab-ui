var description = "## Usage\n\nUse `GlAccordionItem` to place the accordion item within your accordion.\n";

var accordion_item_documentation = {
  description
};

export default accordion_item_documentation;
