var description = "Accordions are used to group similar content and hide or show it depending on user needs or\npreferences. Accordions give users more granular control over the interface and help digest content\nin stages, rather than all at once.\n";

var accordion_documentation = {
  followsDesignSystem: true,
  description
};

export default accordion_documentation;
