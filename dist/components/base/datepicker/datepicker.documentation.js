var description = "Date picker allows users to choose and input a date by manually typing the date into the input field\nor by using a calendar-like dropdown.\n\n### Warning\n\nBe careful when binding a date value using `value` prop. `value` is a watched property and Date\npicker will emit `input` event on _initial load_. Alternatively, use `defaultDate` to set the\ninitial date then receive updated date values through `input` events.\n";

var datepicker_documentation = {
  description
};

export default datepicker_documentation;
