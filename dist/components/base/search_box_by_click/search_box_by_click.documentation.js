var description = "Provides a clearable text input with loading state to be used for [search](https://design.gitlab.com/components/search).\n";

var search_box_by_click_documentation = {
  followsDesignSystem: true,
  description
};

export default search_box_by_click_documentation;
