var description = "Badges highlight metadata of objects, the kind of information that always needs\nsome context and isn’t useful on its own. For example, they can be used to\nindicate an issue’s status, a member’s role, or if a branch is protected.\n\n## Usage\n\n```html\n<gl-badge>Hello, world!</gl-badge>\n```\n\n> Note: Native support for icons in badges will be added in a future version.\n\n### Using icon-only badges\n\nWhen a badge only has an icon and no slot content, be sure to set the `aria-label` attribute of the\nbadge for best accessibility.\n\n```html\n<!-- bad -->\n<gl-badge icon=\"eye\" />\n\n<!-- good -->\n<gl-badge icon=\"eye\" aria-label=\"Mark as confidential\" />\n```\n\n## Edge cases\n\nWhile this component is based on\n[`BBadge`](https://bootstrap-vue.org/docs/components/badge) from\n`bootstrap-vue`, it is not a drop-in replacement. Specifically, this component:\n\n- Has a different set of valid `variant`s. See the examples or props\n   documentation for those values.\n- Always sets the `pill` prop of the underlying `BBadge` to `true`. Any passed\n   in `pill` prop value is ignored.\n- Does _not_ scale itself to match the size of its immediate parent, like\n   `BBadge` does.\n";

var badge_documentation = {
  description,
  followsDesignSystem: true
};

export default badge_documentation;
