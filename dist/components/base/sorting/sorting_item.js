import GlDropdownItem from '../dropdown/dropdown_item';
import GlIcon from '../icon/icon';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

/**
 * Sorting Item
 *
 * This is written as a functional component because it is a simple wrapper over
 * the GlDropdownItem component and does not use internal state. Functional
 * components are cheaper to render and often used as wrappers like this. We're
 * not using the <template functional> syntax here because it does not support
 * custom child components wihtout extra work inside GitLab or extra work
 * required by the user.
 */

var script = {
  name: 'GlSortingItem',
  functional: true,
  props: {
    /**
     * Adds a check mark next to the item to indicate it is active.
     */
    active: {
      type: Boolean,
      default: false,
      required: false
    },

    /**
     * If given, makes the item a link pointing to the given value. Otherwise,
     * the item is a button.
     */
    href: {
      type: String,
      default: null,
      required: false
    }
  },

  /**
   * The content of the item.
   * @slot default
   */
  render(createElement, _ref) {
    let {
      children,
      data,
      props = {}
    } = _ref;
    const classNames = `gl-sorting-item js-active-icon gl-flex-shrink-0 gl-mr-2 ${props.active ? '' : 'inactive gl-visibility-hidden'}`;
    const icon = createElement(GlIcon, {
      class: classNames,
      attrs: {
        name: 'mobile-issue-close',
        size: 16,
        ariaLabel: 'Selected'
      }
    });
    return createElement(GlDropdownItem, { ...data,
      attrs: { ...props
      }
    }, [icon, children]);
  }

};

/* script */
const __vue_script__ = script;

/* template */

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = undefined;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    {},
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
