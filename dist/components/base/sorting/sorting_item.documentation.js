var description = "## Usage\n\nThe sorting item component is meant to be used for clickable entries inside a `gl-sorting` component.\nThis is a wrapper around the `gl-dropdown-item` component and includes a check icon when active,\nand an empty space for alignment when not.\n";

var sorting_item_documentation = {
  description,
  bootstrapComponent: 'b-dropdown-item'
};

export default sorting_item_documentation;
