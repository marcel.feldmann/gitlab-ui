import { GlTooltipDirective } from '../../../directives/tooltip';
import GlButton from '../button/button';
import GlButtonGroup from '../button_group/button_group';
import GlDropdown from '../dropdown/dropdown';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';

var script = {
  name: 'GlSorting',
  components: {
    GlButton,
    GlButtonGroup,
    GlDropdown
  },
  directives: {
    GlTooltip: GlTooltipDirective
  },
  props: {
    /**
     * Text to place in the toggle button.
     */
    text: {
      type: String,
      required: false,
      default: ''
    },

    /**
     * Determines the current sort order icon displayed.
     */
    isAscending: {
      type: Boolean,
      required: false,
      default: false
    },

    /**
     * The text of the tool tip for the sort direction toggle button.
     */
    sortDirectionToolTip: {
      type: String,
      required: false,
      default: 'Sort direction'
    },

    /**
     * Additional class(es) to apply to the root element of the GlDropdown.
     */
    dropdownClass: {
      type: String,
      required: false,
      default: ''
    },

    /**
     * Additional class(es) to apply to the dropdown toggle.
     */
    dropdownToggleClass: {
      type: String,
      required: false,
      default: ''
    },

    /**
     * Additional class(es) to apply to the sort direction toggle button.
     */
    sortDirectionToggleClass: {
      type: String,
      required: false,
      default: ''
    }
  },
  computed: {
    localSortDirection() {
      return this.isAscending ? 'sort-lowest' : 'sort-highest';
    },

    sortDirectionAriaLabel() {
      return this.isAscending ? 'Sorting Direction: Ascending' : 'Sorting Direction: Descending';
    }

  },
  methods: {
    toggleSortDirection() {
      const newDirection = !this.isAscending;
      /**
       * Emitted when the sort direction button is clicked.
       *
       * The event's payload will be true if the direction has been changed to
       * ascending, or false if descending.
       *
       * @property {boolean} isAscending
       */

      this.$emit('sortDirectionChange', newDirection);
    }

  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('gl-button-group',{staticClass:"gl-sorting"},[_c('gl-dropdown',_vm._b({class:_vm.dropdownClass,attrs:{"text":_vm.text,"category":"secondary","toggle-class":_vm.dropdownToggleClass,"right":""}},'gl-dropdown',_vm.$props,false),[_vm._t("default")],2),_vm._v(" "),_c('gl-button',{directives:[{name:"gl-tooltip",rawName:"v-gl-tooltip"}],class:['sorting-direction-button', _vm.sortDirectionToggleClass],attrs:{"title":_vm.sortDirectionToolTip,"icon":_vm.localSortDirection,"aria-label":_vm.sortDirectionAriaLabel},on:{"click":_vm.toggleSortDirection}})],1)};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    undefined,
    undefined,
    undefined
  );

export default __vue_component__;
