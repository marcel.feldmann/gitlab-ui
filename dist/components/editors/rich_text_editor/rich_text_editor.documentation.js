var rich_text_editor = "<!--\nBriefly describe the component's purpose here.\nThis should correspond to the short description in Pajamas' website: https://design.gitlab.com/components/status/\n-->\n\nThe Rich Text Editor is a UI component that provides a WYSIWYG editing\nexperience for[GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.md#gitlab-flavored-markdown-gfm)\n(GFM) in the GitLab application.\nIt also serves as the foundation for implementing Markdown-focused editors that target other engines,\nlike static site generators.\n\n<!-- Provide technical information on how to use the component, add code examples if relevant. -->\n\n<!--\n## Dos and don'ts\n\nIf relevant, describe how the component is expected to be used, and how it's not.\n-->\n\n<!--\n## Browser compatibility\n\nIf the component requires any polyfill or fallback on certain browsers, describe those requirements\nhere.\n-->\n\n<!--\n## Edge cases\n\nIf the component has some known limitations, describe them here.\n-->\n\n<!--\n## Deprecation warning\n\nIf and when this component introduced API changes that would require deprecating old APIs, describe\nthe changes here, and provide a migration paths to the new API.\n-->\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': rich_text_editor
});

var rich_text_editor_documentation = {
  description
};

export default rich_text_editor_documentation;
