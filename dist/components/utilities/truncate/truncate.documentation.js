var description = "The `GlTruncate` component lets you truncate the long texts with ellipsis.\n\n> **Tip:** Try resizing the side panel for truncation.\n\n## Usage\n\n```html\n<gl-truncate :text=\"text\" :position=\"position\" />\n```\n\nBy default, the ellipsis position is at the `end`.\n\nPro Tip: Truncating long filepaths from the `middle` / `start` can help preventing the important\ninformation in the end, i.e. filenames.\n";

var truncate_documentation = {
  description
};

export default truncate_documentation;
