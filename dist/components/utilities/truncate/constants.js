const POSITION = {
  START: 'start',
  MIDDLE: 'middle',
  END: 'end'
};

export { POSITION };
