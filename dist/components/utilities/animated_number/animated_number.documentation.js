var animated_number = "Animated numbers provide a signifier that values are being changed.\n\n## Usage\n\nThe animated number component can be used with or without decimal places. Decimal places will\nnot be rounded if chosen to be omitted.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': animated_number
});

var animated_number_documentation = {
  description,
  followsDesignSystem: false
};

export default animated_number_documentation;
