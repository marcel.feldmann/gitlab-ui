var intersection_observer = "This intersection observer component is an invisible watcher that emits events when it appears and\ndissapears from view.\n\nIt acts a a vue-friendly wrapper around the [intersection observer API](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API).\n\nBecause of it's simplicity you can use it for a lot of different things.\nIt's especially helpful for the lazy loading of images, and infinite scrolling of lists.\n\nAnything slotted inside this component will become the element that is being observed.\n\nThis slot can also be used as a fallback for the browsers that don't support the intersection\nobserver, or in the case that the observer fails to work.\nFor example, adding a \"Fetch more posts\" button inside an observer that should fetch more posts\nautomatically when visible. If the observer fails to work for any reason, the button will still be\nclickable, and the experience preserved. Please use a fallback wherever possible as\n**the intersection observer API is not supported in IE11**.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': intersection_observer
});

var intersection_observer_documentation = {
  description
};

export default intersection_observer_documentation;
