var description = "### Buttons\n\nYou can either have a primary button, a secondary button, or both.\nButtons require both text a link in order for the button to render.\n";

var empty_state_documentation = {
  description
};

export default empty_state_documentation;
