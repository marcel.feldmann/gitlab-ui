var description = "## Dashboard Skeleton\n\nThis is a skeleton loading component for a dashboards page. Renders three cards, but can be adjusted\nbased on the `cards` prop.\n";

var dashboard_skeleton_documentation = {
  description
};

export default dashboard_skeleton_documentation;
