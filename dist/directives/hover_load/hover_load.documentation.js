var hover_load = "A Vue Directive to help with preloading resources when hovering over an element.\n\n## Usage\n\n```html\n<script>\nimport { GlHoverLoadDirective } from '@gitlab/ui';\n\nexport default {\n  directives: { GlHoverLoadDirective },\n  methods: {\n    handlePreload() {\n      fetch('some/endpoint');\n    },\n  },\n};\n</script>\n\n<template>\n  <div v-gl-hover-load=\"handlePreload\">Hover me to preload</div>\n</template>\n```\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': hover_load
});

var hover_load_documentation = {
  description,
  followsDesignSystem: false
};

export default hover_load_documentation;
