var outside = "A Vue Directive to call a callback when a click occurs *outside* of the element\nthe directive is bound to. Any clicks on the element or any descendant elements are ignored.\n\n## Usage\n\n```html\n<script>\nimport { GlOutsideDirective as Outside } from '@gitlab/ui';\n\nexport default {\n  directives: { Outside },\n  methods: {\n    onClick(event) {\n      console.log('User clicked somewhere outside of this component', event);\n    },\n  },\n};\n</script>\n\n<template>\n  <div v-outside=\"onClick\">Click anywhere but here</div>\n</template>\n```\n\n## Caveats\n\n- If a click event is stopped (e.g., via `event.stopPropagation()`) before it\n  bubbles up to the `document`, it cannot be detected by `GlOutsideDirective`.\n- Clicks cannot be detected across document boundaries (e.g., across an\n  `iframe` boundary), in either direction.\n";

var description = /*#__PURE__*/Object.freeze({
  __proto__: null,
  'default': outside
});

var outside_documentation = {
  description,
  followsDesignSystem: false
};

export default outside_documentation;
