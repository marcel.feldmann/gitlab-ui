// See https://gitlab.com/gitlab-org/gitlab-ui/-/issues/1421#note_617098438
// for more details
const forbiddenDataAttrs = ['data-remote', 'data-url', 'data-type', 'data-method'];

export { forbiddenDataAttrs };
