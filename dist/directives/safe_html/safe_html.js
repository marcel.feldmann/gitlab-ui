import DOMPurify from 'dompurify';
import { forbiddenDataAttrs } from './constants';

// avoiding additional serialize/parse round trip.
// See https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1782
// and https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/2127
// for more details.

const DEFAULT_CONFIG = {
  RETURN_DOM_FRAGMENT: true,
  FORBID_ATTR: [...forbiddenDataAttrs]
};

const transform = (el, binding) => {
  if (binding.oldValue !== binding.value) {
    var _binding$arg;

    const config = { ...DEFAULT_CONFIG,
      ...((_binding$arg = binding.arg) !== null && _binding$arg !== void 0 ? _binding$arg : {})
    };
    el.textContent = '';
    el.appendChild(DOMPurify.sanitize(binding.value, config));
  }
};

const SafeHtmlDirective = {
  bind: transform,
  update: transform
};

export { SafeHtmlDirective };
