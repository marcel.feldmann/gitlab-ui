var description = "This directive can be used to get notified whenever a given element's size (width or height) changes\nand to retrieve the updated dimensions.\n\nUnder the hood, it leverages the [Resize Observer API](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver).\nIf you use GitLab UI in an older browser which doesn't support the Resize Observer API,\nyou can use a [polyfill](https://github.com/que-etc/resize-observer-polyfill).\n\nThe directive accepts a callback as a value and passes on the received\n[contentRect](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserverEntry/contentRect)\nand the target element whenever a resize event gets triggered.\n\n```html\n<script>\nexport default {\n  data() {\n    return {\n      width: 0,\n      height: 0,\n    };\n  },\n  methods: {\n    handleResize({ contentRect: { width, height } }) {\n      this.width = width;\n      this.height = height;\n    },\n  },\n};\n</script>\n<template>\n  <div v-gl-resize-observer-directive=\"handleResize\">\n    <p>{{ width }} x {{ height }}</p>\n  </div>\n</template>\n```\n\nThe observer can be toggled on or off by passing a boolean argument to the directive:\n\n```html\n<script>\nexport default {\n  data() {\n    return {\n      shouldObserve: true,\n    };\n  },\n  methods: {\n    handleResize() {},\n  },\n};\n</script>\n<template>\n  <div v-gl-resize-observer-directive[shouldObserve]=\"handleResize\"></div>\n</template>\n```\n";

var resize_observer_documentation = {
  followsDesignSystem: false,
  description
};

export default resize_observer_documentation;
