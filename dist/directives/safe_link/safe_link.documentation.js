var description = "A Vue directive to make the hyperlinks secure by default.\n\n## Security measures\n\n### rel\n\nWhen setting target to `_blank`, the rel attribute gets set automatically to `noopener noreferrer`,\nthis is done to avoid the `window.opener` [API exploit]. If you set the `rel` attribute manually,\nthis will overwrite the aforementioned logic.\n\n### href\n\nThis directive enforces \"safe\" URLs. What this means is that, if the provided `href`\ndoesn't point to a safe protocol (one of `http:`, `https:`, `mailto:` or `ftp:`), then it is\nreplaced with `about:blank` to prevent [URL injections].\n\n```html\n<script>\nimport { GlSafeLinkDirective as SafeLink } from '@gitlab/ui';\n\nexport default {\n  data() {\n    return {\n      url: 'javascript:alert(1)',\n    };\n  },\n  directives: { SafeLink },\n};\n</script>\n<template>\n  <a v-safe-link href=\"url\" target=\"_blank\">Click</a>\n  <!-- Renders to: <a href=\"about:blank\" target=\"_blank\">Click</a> -->\n</template>\n```\n\n[API exploit]: https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/\n[URL injections]: https://vuejs.org/v2/guide/security.html#Injecting-URLs\n";

var safe_link_documentation = {
  followsDesignSystem: false,
  description
};

export default safe_link_documentation;
