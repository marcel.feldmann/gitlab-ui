/**
 * Adds two numbers together
 * @param {Number} a
 * @param {Number} b
 */
const addition = (a, b) => a + b;
/**
 * Returns the sum of all arguments
 * @param  {...Number} numbers
 */

const sum = function () {
  for (var _len = arguments.length, numbers = new Array(_len), _key = 0; _key < _len; _key++) {
    numbers[_key] = arguments[_key];
  }

  return numbers.reduce(addition);
};
/**
 * Returns the average of all arguments
 * @param  {...Number} numbers
 */

const average = function () {
  return sum(...arguments) / arguments.length;
};
/**
 * Convert number to engineering format, using SI suffix
 * @param {Number|String} value - Number or Number-convertible String
 * @param {Number} precision
 * @return {String} number, possibly with a suffix
 */

const engineeringNotation = function (value) {
  let precision = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  const invalidValues = [NaN, Infinity, -Infinity];
  const num = Number(value);

  if (invalidValues.includes(num) || invalidValues.includes(Number(precision))) {
    return num.toString();
  }

  const allYourBase = {
    '-24': 'y',
    '-21': 'z',
    '-18': 'a',
    '-15': 'f',
    '-12': 'p',
    '-9': 'n',
    '-6': 'μ',
    '-3': 'm',
    0: '',
    3: 'k',
    6: 'M',
    9: 'G',
    12: 'T',
    15: 'P',
    18: 'E',
    21: 'Z',
    24: 'Y'
  };
  const exponentialNotation = num.toExponential(precision);
  const power = exponentialNotation.split('e').map(Number)[1] || 0;

  if (power < -24 || power > 24) {
    return exponentialNotation;
  }

  const scaledPower = 3 * Math.floor(power / 3);
  const scaledMantissa = (exponentialNotation / 10 ** scaledPower // strip trailing decimals from floating point rounding errors
  ).toFixed(precision) // strip trailing 0s after a decimal point
  .replace(/\.([1-9]*)0+$/, (_, fractionalDigits) => {
    if (fractionalDigits) {
      return `.${fractionalDigits}`;
    }

    return '';
  });
  return `${scaledMantissa}${allYourBase[scaledPower]}`;
};

export { addition, average, engineeringNotation, sum };
