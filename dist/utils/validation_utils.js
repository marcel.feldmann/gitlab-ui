// Creates out of validation configurations from .documentation.js files and creates a readable string out of it
const getValidationInfoText = function () {
  let validation = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  switch (validation.type) {
    case 'range':
      return `${validation.min}-${validation.max}`;

    default:
      return '';
  }
};

export { getValidationInfoText };
