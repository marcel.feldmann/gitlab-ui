import { POSITION } from '../components/utilities/truncate/constants';

const glThemes$1 = 'indigo ,  blue ,  light-blue ,  green ,  red ,  light-red';
const glIconSizes = '8 12 14 16 24 32 48 72';

function appendDefaultOption(options) {
  return { ...options,
    default: ''
  };
}

const COMMA = ',';
const glThemes = glThemes$1.split(COMMA).map(glTheme => glTheme.trim());
const variantOptions = {
  primary: 'primary',
  secondary: 'secondary',
  success: 'success',
  warning: 'warning',
  danger: 'danger',
  info: 'info',
  light: 'light',
  dark: 'dark'
};
const badgeSizeOptions = {
  sm: 'sm',
  md: 'md',
  lg: 'lg'
};
const badgeVariantOptions = {
  muted: 'muted',
  neutral: 'neutral',
  info: 'info',
  success: 'success',
  warning: 'warning',
  danger: 'danger'
};
const variantCssColorMap = {
  muted: 'gl-text-gray-500',
  neutral: 'gl-text-blue-100',
  info: 'gl-text-blue-500',
  success: 'gl-text-green-500',
  warning: 'gl-text-orange-500',
  danger: 'gl-text-red-500'
};
const targetOptions = ['_self', '_blank', '_parent', '_top', null];
const sizeOptions = {
  default: null,
  sm: 'sm',
  lg: 'lg'
};
const labelSizeOptions = {
  default: null,
  sm: 'sm'
};
const viewModeOptions = {
  dark: 'dark',
  light: 'light'
};
const labelColorOptions = { ...viewModeOptions
};
const avatarSizeOptions = [96, 64, 48, 32, 24, 16];
const avatarsInlineSizeOptions = [32, 24];
const avatarShapeOptions = {
  circle: 'circle',
  rect: 'rect'
};
const formStateOptions = {
  default: null,
  valid: true,
  invalid: false
};
const buttonCategoryOptions = {
  primary: 'primary',
  secondary: 'secondary',
  tertiary: 'tertiary'
};
const buttonVariantOptions = {
  default: 'default',
  confirm: 'confirm',
  info: 'info (deprecated)',
  success: 'success (deprecated)',
  warning: 'warning (deprecated)',
  danger: 'danger',
  dashed: 'dashed',
  link: 'link',

  /**
   * The "reset" variant can be used when customization of GlButton styles is required
   * (e.g. for the "close" button in GlLabel).
   * It should be used sparingly and only when other approaches fail.
   * Prefer supported variants where ever possible.
   */
  reset: 'gl-reset'
};
const badgeForButtonOptions = {
  [buttonVariantOptions.default]: badgeVariantOptions.neutral,
  [buttonVariantOptions.confirm]: badgeVariantOptions.info,
  [buttonVariantOptions.danger]: badgeVariantOptions.danger
};
const dropdownVariantOptions = {
  default: 'default',
  confirm: 'confirm',
  info: 'info (deprecated)',
  success: 'success (deprecated)',
  warning: 'warning (deprecated)',
  danger: 'danger',
  link: 'link'
};
const buttonSizeOptions = {
  small: 'small',
  medium: 'medium'
};
const buttonSizeOptionsMap = {
  small: 'sm',
  medium: 'md'
}; // size options all have corresponding styles (e.g. .s12 defined in icon.scss)

const iconSizeOptions = glIconSizes.split(' ').map(Number);
const triggerVariantOptions = {
  click: 'click',
  hover: 'hover',
  focus: 'focus'
};
const tooltipPlacements = {
  top: 'top',
  left: 'left',
  right: 'right',
  bottom: 'bottom'
}; // in milliseconds

const tooltipDelay = {
  show: 500,
  hide: 0
};
const popoverPlacements = {
  top: 'top',
  right: 'right',
  bottom: 'bottom',
  left: 'left'
};
const columnOptions = {
  stacked: 'stacked',
  tiled: 'tiled'
};
const alignOptions = {
  left: 'left',
  center: 'center',
  right: 'right',
  fill: 'fill'
};
const alertVariantOptions = {
  success: 'success',
  warning: 'warning',
  danger: 'danger',
  info: 'info',
  tip: 'tip'
};
const alertVariantIconMap = {
  success: 'check-circle',
  warning: 'warning',
  danger: 'error',
  info: 'information-o',
  tip: 'bulb'
};
const colorThemes = {
  indigo: 'theme-indigo-900',
  'light-indigo': 'theme-indigo-700',
  blue: 'theme-blue-900',
  'light-blue': 'theme-blue-700',
  green: 'theme-green-900',
  'light-green': 'theme-green-700',
  red: 'theme-red-900',
  'light-red': 'theme-red-700',
  dark: 'gray-900',
  light: 'gray-700'
};
const modalButtonDefaults = {
  actionPrimary: {
    variant: 'confirm',
    category: 'primary'
  },
  actionSecondary: {
    variant: 'confirm',
    category: 'secondary'
  },
  actionCancel: {
    variant: 'default'
  }
};
const tabsButtonDefaults = {
  actionPrimary: {
    variant: 'success',
    category: 'primary'
  },
  actionSecondary: {
    variant: 'default',
    category: 'secondary'
  },
  actionTertiary: {
    variant: 'default'
  }
};
const tokenVariants = ['default', 'search-type', 'search-value'];
const resizeDebounceTime = 200;
const variantOptionsWithNoDefault = appendDefaultOption(variantOptions);
const sizeOptionsWithNoDefault = appendDefaultOption(sizeOptions); // Datetime constants

const defaultDateFormat = 'YYYY-MM-DD';
const bannerVariants = ['promotion', 'introduction'];
const maxZIndex = 10;
const modalSizeOptions = {
  sm: 'sm',
  md: 'md',
  lg: 'lg'
};
const focusableTags = ['INPUT', 'TEXTAREA', 'A', 'BUTTON', 'SELECT'];
const keyboard = {
  escape: 'Escape',
  backspace: 'Backspace',
  delete: 'Delete',
  left: 'Left',
  arrowLeft: 'ArrowLeft',
  right: 'Right',
  arrowRight: 'ArrowRight',
  home: 'Home',
  end: 'End'
};
const truncateOptions = POSITION;
const formInputSizes = {
  xs: 'xs',
  sm: 'sm',
  md: 'md',
  lg: 'lg',
  xl: 'xl',
  '(unset or null)': null
};
const toggleLabelPosition = {
  hidden: 'hidden',
  left: 'left',
  top: 'top'
};
const tooltipActionEvents = ['open', 'close', 'enable', 'disable'];
const drawerVariants = {
  default: 'default',
  sidebar: 'sidebar'
};
const loadingIconSizes = {
  'sm (16x16)': 'sm',
  'md (24x24)': 'md',
  'lg (32x32)': 'lg',
  'xl (64x64)': 'xl'
};

export { COMMA, alertVariantIconMap, alertVariantOptions, alignOptions, avatarShapeOptions, avatarSizeOptions, avatarsInlineSizeOptions, badgeForButtonOptions, badgeSizeOptions, badgeVariantOptions, bannerVariants, buttonCategoryOptions, buttonSizeOptions, buttonSizeOptionsMap, buttonVariantOptions, colorThemes, columnOptions, defaultDateFormat, drawerVariants, dropdownVariantOptions, focusableTags, formInputSizes, formStateOptions, glThemes, iconSizeOptions, keyboard, labelColorOptions, labelSizeOptions, loadingIconSizes, maxZIndex, modalButtonDefaults, modalSizeOptions, popoverPlacements, resizeDebounceTime, sizeOptions, sizeOptionsWithNoDefault, tabsButtonDefaults, targetOptions, toggleLabelPosition, tokenVariants, tooltipActionEvents, tooltipDelay, tooltipPlacements, triggerVariantOptions, truncateOptions, variantCssColorMap, variantOptions, variantOptionsWithNoDefault, viewModeOptions };
