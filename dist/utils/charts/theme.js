import { scrollHandleSvgPath } from '../svgs/svg_paths';
import { hexToRgba } from '../utils';

const whiteNormal = '#f0f0f0';
const red500 = '#dd2b0e';
const gray50 = '#f0f0f0';
const gray100 = '#dbdbdb';
const gray200 = '#bfbfbf';
const gray300 = '#999';
const gray500 = '#666';
const gray600 = '#5e5e5e';
const gray700 = '#525252';
const gray900 = '#303030';
const dataVizGreen500 = '#608b2f';
const dataVizGreen600 = '#487900';
const dataVizGreen700 = '#366800';
const dataVizGreen800 = '#275600';
const dataVizGreen900 = '#1a4500';
const dataVizGreen950 = '#0f3300';
const dataVizAqua500 = '#0094b6';
const dataVizAqua600 = '#0080a1';
const dataVizAqua700 = '#006887';
const dataVizAqua800 = '#004d67';
const dataVizAqua900 = '#003f57';
const dataVizAqua950 = '#00293d';
const dataVizBlue200 = '#b7c6ff';
const dataVizBlue400 = '#748eff';
const dataVizBlue500 = '#5772ff';
const dataVizBlue600 = '#445cf2';
const dataVizBlue700 = '#3547de';
const dataVizBlue800 = '#232fcf';
const dataVizBlue900 = '#1e23a8';
const dataVizBlue950 = '#11118a';
const dataVizMagenta500 = '#d84280';
const dataVizMagenta600 = '#c52c6b';
const dataVizMagenta700 = '#b31756';
const dataVizMagenta800 = '#950943';
const dataVizMagenta900 = '#7a0033';
const dataVizMagenta950 = '#570028';
const dataVizOrange500 = '#d14e00';
const dataVizOrange600 = '#b24800';
const dataVizOrange700 = '#944100';
const dataVizOrange800 = '#6f3500';
const dataVizOrange900 = '#5c2b00';
const dataVizOrange950 = '#421e00';

const themeName = 'gitlab';
const heatmapHues = [gray100, dataVizBlue200, dataVizBlue400, dataVizBlue600, dataVizBlue800];
const gaugeNeutralHues = [gray900, gray500];
const gaugeSafeHues = [dataVizBlue500, dataVizBlue900];
const gaugeWarningHue = dataVizOrange500;
/**
 * The default palette is based on the Categorical data palette
 * Categorical data (also known as qualitative or thematic) uses hue to
 * differentiate qualitative data, and lightness to differentiate quantitive data.
 * More info: https://gitlab.com/gitlab-org/gitlab-design/-/issues/719#categorical
 */

const colorPaletteDefault = [dataVizBlue500, dataVizOrange600, dataVizAqua500, dataVizGreen700, dataVizMagenta800, dataVizBlue700, dataVizOrange800, dataVizAqua700, dataVizGreen900, dataVizMagenta950, dataVizBlue900, dataVizOrange950, dataVizAqua900, dataVizGreen600, dataVizMagenta700, dataVizBlue600, dataVizOrange700, dataVizAqua600, dataVizGreen800, dataVizMagenta900, dataVizBlue800, dataVizOrange900, dataVizAqua800, dataVizGreen950, dataVizMagenta500, dataVizBlue950, dataVizOrange500, dataVizAqua950, dataVizGreen500, dataVizMagenta600];
const colorFromDefaultPalette = index => colorPaletteDefault[index % colorPaletteDefault.length];
const axes = {
  axisLabel: {
    margin: 8,
    show: true,
    color: gray600,
    hideOverlap: true
  },
  axisLine: {
    show: false
  },
  axisPointer: {
    lineStyle: {
      type: 'solid',
      color: gray600
    },
    label: {
      show: false
    }
  },
  axisTick: {
    show: true,
    alignWithLabel: true,
    lineStyle: {
      color: gray200
    }
  },
  nameGap: 30,
  nameTextStyle: {
    fontStyle: 'bold'
  },
  splitLine: {
    lineStyle: {
      color: [gray200]
    }
  },
  splitArea: {
    show: false,
    areaStyle: {
      color: [hexToRgba(whiteNormal, 0.3), hexToRgba(gray300, 0.3)]
    }
  }
};

const isLineChartWithoutArea = options => Array.isArray(options === null || options === void 0 ? void 0 : options.series) && options.series.some(series => series.type === 'line' && !series.areaStyle);

const createTheme = function () {
  let options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return {
    color: colorPaletteDefault,
    backgroundColor: 'transparent',
    textStyle: {
      color: gray900
    },
    markLine: {
      silent: true,
      symbol: 'none',
      label: {
        show: false
      },
      lineStyle: {
        color: red500,
        width: 1,
        type: 'dashed'
      }
    },
    markArea: {
      silent: true,
      itemStyle: {
        color: hexToRgba(red500, 0.1)
      }
    },
    dataZoom: {
      borderColor: 'transparent',
      filterMode: 'none',
      brushSelect: false,
      dataBackground: {
        lineStyle: {
          width: 2,
          color: gray200,
          opacity: 1
        },
        // render unfilled zoom-graph if the series is a line chart without area styles
        // more details: https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/2364#note_666637306
        areaStyle: isLineChartWithoutArea(options) ? {} // Use empty object instead of null, see https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/2185#note_707711029 for more context
        : {
          color: gray50,
          opacity: 1
        }
      },
      fillerColor: hexToRgba(gray200, 0.23),
      handleIcon: scrollHandleSvgPath,
      handleStyle: {
        borderColor: 'transparent',
        color: gray500
      },
      handleSize: '50%',
      labelFormatter: () => null,
      textStyle: {
        color: gray600
      }
    },
    toolbox: {
      top: '-5',
      left: 'center',
      itemSize: 14,
      emphasis: {
        iconStyle: {
          borderWidth: 0,
          color: gray700
        }
      },
      iconStyle: {
        color: gray200,
        borderWidth: 0
      },
      itemGap: 8,
      feature: {
        dataZoom: {
          title: {
            zoom: 'Click to zoom in on a portion of the graph',
            back: 'Remove selection'
          }
        },
        restore: {
          title: 'Remove all selections and return chart to default state'
        },
        saveAsImage: {
          title: 'Save chart as an image',
          name: 'graph'
        }
      }
    },
    markPoint: {
      label: {
        normal: {
          textStyle: {
            color: whiteNormal
          }
        },
        emphasis: {
          textStyle: {
            color: whiteNormal
          }
        }
      }
    },
    line: {
      itemStyle: {
        normal: {
          borderWidth: 1
        }
      },
      lineStyle: {
        normal: {
          width: 2
        }
      },
      symbolSize: '6',
      symbol: 'circle',
      showSymbol: false,
      smooth: false
    },
    categoryAxis: axes,
    valueAxis: axes,
    logAxis: axes,
    timeAxis: axes
  };
};

export default createTheme;
export { colorFromDefaultPalette, colorPaletteDefault, gaugeNeutralHues, gaugeSafeHues, gaugeWarningHue, heatmapHues, themeName };
